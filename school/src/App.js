import './App.css';
import { Route, Routes, useNavigate } from 'react-router-dom';
import LoginPage from './components/LoginPage';
import HomePage from './components/HomePage';
import AdminPage from './components/admin/AdminPage';
import { Component, useEffect, useState } from 'react';
import { CircleProgress } from 'react-gradient-progress';
import logo_shadow from './img/logo_shadow.png'
import logo from './img/logo.png'
import AddNewSuperUserAdminForm from './AddNewSuperUserAdminForm';

class Progress extends Component {
  state ={  
    pourcentage:0
  }
  componentDidMount() {
    setInterval(() => {
      if(this.props.isLoading){
        this.setState({pourcentage:this.state.pourcentage+3})
        if (this.state.pourcentage >= 100 ){
          this.setState({pourcentage:0})
          if(this.props.isRedirected=== false){
            window.location.reload()
          }
        }
      }
    }, 100);
  }
 render() {
   return (
         <div className="container-fluid" >         
                 <div id='progress_circle' className='mx-auto' style={{width:'200px',height:'200px'}}>
                  <div className='m-auto row justify-content-center'>
                    <img src={logo_shadow} style={{width:"300px",position:'absolute',marginTop:'-38px'}}></img>
                    <img src={logo} style={{width:"150px",position:'absolute',marginTop:'37px'}}></img>
                  </div>
                  <CircleProgress fontColor='rgba(0,0,0,0)' secondaryColor={'#006bbd'} strokeLinecap='round' strokeWidth={15} fontSize='30px' primaryColor={['#2dcdcf', '#2dcdcf']}  percentage={this.state.pourcentage} />

                 </div>
         </div>
   )
 }
}
function App() {
    
      let [isLoading,setIsLoading] = useState(true)
      let isRedirected = false
      let navigateUser= useNavigate()
      useEffect(()=>{
        let currentLocation = window.location.toString()
        if(currentLocation.substring(currentLocation.length-20,currentLocation.length)==='addNewSuperUserAdmin'){
          setIsLoading(false)
        }else{
          navigateUser('/')
          setTimeout(() => {
            setIsLoading(false)
            navigateUser('/login')
          }, 2000);
        }
      },[])
      // ######################################################//
      return (
        <div className="App m-0 p-0" style={{overflowX:'hidden'}} >
          <Routes>
              <Route path="/*" element={
                <div className=' m-0 bg-App'>
                  <div>
                    <Progress isLoading={isLoading} isRedirected={isRedirected} ></Progress>
                  </div>
                </div>
              }></Route>
              <Route path="/addNewSuperUserAdmin" element={<AddNewSuperUserAdminForm/> }></Route>
              <Route path="/login/*" element={<LoginPage/> }></Route>
              <Route path="home/*" element={<HomePage/> }>
                <Route path="admin/*" element={<AdminPage></AdminPage> }></Route>
              </Route>
          </Routes>
        </div>
      ); 
}
export default App;