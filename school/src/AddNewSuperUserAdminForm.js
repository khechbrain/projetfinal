import React, { Component } from 'react';
import {doc,setDoc,collection, addDoc, onSnapshot} from 'firebase/firestore';
import { firestore } from './utils/firebase';


class AddNewSuperUserAdminForm extends Component {

    state={
            statut:"admin",
            fName:"",
            lName:"",
            displayName:"",
            number:"",
            email:"",
            fonction:"seceretary",
            salary:"0",
            password:"",
            confirmPassword:"",
            formation:"default",
            fDuration:"default",
            imageUrl: "https://firebasestorage.googleapis.com/v0/b/projet-final-bakeli.appspot.com/o/users%2Fprofiles%2Fdefault_profile_img_for_all_users.png?alt=media",
            isAdmin: true,
            allUsersEmails:[],
            hasError:false,
            errorMsg:""
    }
    componentDidMount() {
        onSnapshot(collection(firestore,"users"),(snapshot)=>{
            snapshot.forEach((snap)=>{
                this.setState({allUsersEmails:[...this.state.allUsersEmails,snap.data().email]})
            })
        })
    }
    
    render(){
        const verification=()=>{
                if(this.state.number.length !== 9 || this.state.number.substring(0,2)!=='77'&&
                this.state.number.substring(0,2)!=='78'&& this.state.number.substring(0,2)!=='76'&&
                this.state.number.substring(0,2)!=='70'){
                    this.setState({
                        hasError:true,
                        errorMsg:"Numéro de Téléphone invalid"
                    })
                    return false
                }else if(this.state.password !== this.state.confirmPassword){
                    this.setState({
                        hasError:true,
                        errorMsg:"Les deux mots de passes ne se correspondent pas"
                    })
                    return false
                }else if(this.state.statut==='default'){
                    this.setState({
                        hasError:true,
                        errorMsg:"Vous deveez d'abord choisir la statut de l'utilisateur"
                    })
                    return false
                }else if(this.state.statut==='admin' && this.state.fonction==="default"){
                    this.setState({
                        hasError:true,
                        errorMsg:"Vous deveez d'abord choisir la fonction de l'utilisateur"
                    })
                    return false
                }else if(this.state.statut==='student' && this.state.formation==="default"){
                    this.setState({
                        hasError:true,
                        errorMsg:"Vous deveez d'abord choisir le domaine de formation de l'étudiant"
                    })
                    return false
                }else{
                    let isFound = false
                    this.state.allUsersEmails.map((email,index)=>{
                        if(email===this.state.email){
                            this.setState({
                                hasError:true,
                                errorMsg:"l'email \""+this.state.email+"\" déja utilisé par un autre utilisateur"
                            })
                            isFound =true
                            return false
                        }
                    })
                    if(!isFound){
                        // Pret pour valider le formulaire
                        this.setState({hasError:false})
                        return true
                    }
                }

        }
        const addUser =(e)=>{
            e.preventDefault(); // Stop the form from submitting
            let user = this.state.isAdmin? ({
                statut:this.state.statut,
                fName:this.state.fName,
                lName:this.state.lName,
                displayName:this.state.fName+' '+this.state.lName,
                number:this.state.number,
                email:this.state.email,
                fonction:this.state.fonction,
                imageUrl: this.state.imageUrl,
                salary:this.state.salary,
                password:this.state.password
            })
            :
            ({
                statut:this.state.statut,
                fName:this.state.fName,
                lName:this.state.lName,
                displayName:this.state.fName+' '+this.state.lName,
                number:this.state.number,
                email:this.state.email,
                formation:this.state.formation,
                fDuration:this.state.fDuration,
                imageUrl: this.state.imageUrl,
                password:this.state.password
            })
            if(verification()){
                // Si la vérification renvoie true
                // On peut ajouter le nouveau utilisateur
                addDoc(collection(firestore,"users"),user)
                .then((reult)=>{
                    window.location = window.location.toString().substring(0,window.location.toString().length-17)
                }).catch((err)=>alert(err))
            }
        }
        return (
            <div className='m-2 m-md-5'>
                <h3 className='titre-theme'>Créer un compte d'utilisateur admin:</h3>
                <div className='p-2 shadow' style={{border:'4px solid #2dcdcf',borderRadius:'10px'}}>
            
                    <form onSubmit={e=>addUser(e)}>
                        <div className={this.state.hasError?'d-flex bg-warning row m-1 rounded':'d-none'}>
                            <p className='col-11'>{this.state.errorMsg}</p>
                            <button type='button' className='col-1 btn-danger rounded' onClick={()=>this.setState({hasError:false})}>X</button>
                        </div>
                        <div className='row justify-content-center my-1'>
                            <div className='col-12 col-sm-6 justify-content-start'>
                                <div className='input-group col-6'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Prénom:</label>
                                    <input type="text" required className='col-9 my-2 rounded form-control' placeholder='Prénom' value={this.state.fName} onChange={e=>this.setState({fName:e.target.value})}></input>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 '>
                                <div className='input-group col-6'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Nom:</label>
                                    <input type="text" required className='col-9 my-2 rounded form-control' placeholder='Nom' value={this.state.lName} onChange={e=>this.setState({lName:e.target.value})}></input>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 '>
                                <div className='input-group col-6'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Numéro:</label>
                                    <input type="number" required className='col-9 my-2 rounded form-control' placeholder='Numéro de téléphone' value={this.state.number} onChange={e=>this.setState({number:e.target.value})}></input>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 '>
                                <div className='input-group col-6'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Email:</label>
                                    <input type="email" required className='col-9 my-2 rounded form-control' placeholder='Email' value={this.state.email} onChange={e=>this.setState({email:e.target.value})}></input>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 col-md-9 col-lg-6 '>
                                <div className='input-group col-6'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Mot de passe:</label>
                                    <input type="password" required className='col-9 my-2 rounded form-control' placeholder='Mot de passe' value={this.state.password} onChange={e=>this.setState({password:e.target.value})}></input>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 col-md-9 col-lg-6 '>
                                <div className='input-group col-6'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Confirmer le Mot de passe:</label>
                                    <input type="password" required className='col-9 my-2 rounded form-control' placeholder='Mot de passe' value={this.state.confirmPassword} onChange={e=>this.setState({confirmPassword:e.target.value})}></input>
                                </div>
                            </div>
                        </div>
                        <button type='submit' className='btn col-10 col-sm-6 col-md-8 col-lg-6  btn-dark my-2' style={{zIndex:'1'}}>Créer un utilisateur admin</button>
                    </form>
                </div>
            </div>
        );
    }
}

export default AddNewSuperUserAdminForm;