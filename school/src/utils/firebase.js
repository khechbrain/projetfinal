// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore,onSnapshot, collection } from "firebase/firestore";
import { getDatabase,onValue, ref } from "firebase/database";
import { getAuth} from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDrmWoLHI_ITNcr5Y1UfRTrchT8Zg177-o",
  authDomain: "projet-final-bakeli.firebaseapp.com",
  projectId: "projet-final-bakeli",
  storageBucket: "projet-final-bakeli.appspot.com",
  messagingSenderId: "638700613498",
  appId: "1:638700613498:web:b65d77eef7cd0b915430c3"
};
// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const firestore = getFirestore(app)
export const auth = getAuth(app)

export let currentUser ={}
export let currentUserId =""
export let msgError ={}

 export function setCurrentUser(newUser){
   
   onSnapshot(collection(firestore,"users"),(snapshot)=>{
     let isFound = false
     snapshot.forEach((snap)=>{
       let user = snap.data()

       if(user.email===newUser.email){
         if(user.password === newUser.password){
           currentUser = user
           currentUserId = snap.id
           isFound = true
         }else{
           msgError = 'Mot de passe incorrect'
           alert(msgError)
         }
       }
     })
     if(!isFound){
      //  msgError = 'Adress email introuvable ou connextion internet trop faible'
      //  alert(msgError)
     }
   })
 }