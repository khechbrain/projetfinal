import React, { Component } from 'react';
import { CircleProgress } from 'react-gradient-progress';

var showed = false

export class WaitingToast extends Component {
    state={
        pourcentage:50,
        // showed:false
    }
    render() {
        return (
            <div className={showed?'d-flex':'d-none'} style={{position:'fixed',background:'rgba(50,50,100,0.3)',width:'100vw',height:'100vh',zIndex:'100',display:'flex',justifyContent:'center',alignItems:'center'}}>
                <CircleProgress fontColor='rgba(0,0,0,0)' secondaryColor={'#006bbd'} strokeLinecap='round' strokeWidth={15} fontSize='30px' primaryColor={['#2dcdcf', '#2dcdcf']}  percentage={this.state.pourcentage} />
            </div>
        );
    }
}
export const showToast=(toastBoolean)=>{
    showed=toastBoolean
}
