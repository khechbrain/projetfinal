import React, { Component, useEffect, useState } from 'react';
import {app,currentUser,setCurrentUser} from '../utils/firebase';
import { Link, Route, Routes, useNavigate } from 'react-router-dom';
import {BsFillEyeFill} from 'react-icons/bs'
import {BsFillEyeSlashFill} from 'react-icons/bs'
import {BsEnvelope} from 'react-icons/bs'
import {FiLock} from 'react-icons/fi'

import logo from '../img/logo.png'
import ResetPassword from './ResetPassword';

class Verificator extends Component {
    constructor(props) {
      super(props);
      
      this.state = {
          email:'',
          password:'',
          showPassWord:''
      }
    }
    render() {
        const submit =(e)=>{
            e.preventDefault(); // Stop the form from submitting
            // showToast(true)
            let navigate = this.props.navigate
            let newUser = {
                email:this.state.email,
                password:this.state.password
            }
            setCurrentUser(newUser)

            setTimeout(() => {
                if(currentUser.email === newUser.email){
                    navigate("/home")
                }
            }, 2000);
        }
        return (
            <div className='row bg-light justify-content-center' style={{height:'100vh'}} >                 
               
                <div className='col-6 log_img d-none d-md-block' >
                   <div style={{marginTop:'35%'}}>
                       <h1 className='text-light' style={{textShadow:'0 0 5px #006bbd',textAlign:'center',fontSize:'400%',fontFamily:'poppins'}}>Daradji Tech</h1>
                       <h3 className='text-light' style={{textShadow:'0 0 5px #006bbd',textAlign:'center',fontFamily:'poppins'}}>Ecole de formation 100% pratique</h3>
                   </div>   
                   
                </div>
                <div className=' col-12 col-md-6 row justify-content-center my-5'>
                    <div className='d-inline row mx-auto' style={{width:'200px'}}>
                        <img src={logo}/>
                    </div>
                    <div className='row mx-auto'>
                        <form onSubmit={e=>submit(e)} className='mx-auto col-12 col-md-8 form row t p-3'>
                            <h2>Connexion:</h2>
                            <p>Veuillez vous connecter SVP</p>

                            <div className='input-group mb-3 shadow rounded'>
                              <span className='input-group-text ' style={{marginLeft:'0',height:'40px',marginTop:'8px',color:'blue'}}> <BsEnvelope /> </span>
                              <input type="email" required className='my-2 form-control' placeholder='Email' value={this.state.email} onChange={e=>this.setState({email:e.target.value})}></input>
                            </div>
                            <div className='input-group mb-3 shadow rounded'>
                                <span className='input-group-text ' style={{marginLeft:'0',height:'40px',marginTop:'8px',color:'blue'}}> <FiLock /> </span>
                                <input  type={this.state.showPassWord ? "text" :"password"} required className='my-2 form-control' placeholder='Mot de passe' value={this.state.password} onChange={e=>this.setState({password:e.target.value})}></input>
                                <button type='button' className='input-group-text disable' style={{height:'40px',marginTop:'8px',color:'blue'}} onClick={()=>this.setState({showPassWord:!this.state.showPassWord})}> {this.state.showPassWord ? < BsFillEyeFill/>:< BsFillEyeSlashFill/>}</button>
                            </div>
                            
                            <button type='submit' className='btn btn-dark my-2'>Se connecter</button>
                            <p>Mot de passe oublié? cliquez <Link to="resetPassword">ici</Link></p>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
  }
  
const LoginPage = () => {
    let navigate = useNavigate()
    return  (
        <>
            
            <Routes>
                <Route path='' element={
                    <Verificator navigate={navigate}></Verificator>
                }></Route>
                <Route path='resetPassword' element={<ResetPassword></ResetPassword>}></Route>
            </Routes>
        </>
    )
};

export default LoginPage;