import React from 'react';
import { Link } from 'react-router-dom';
import { currentUser } from '../../utils/firebase';
import './MenuBar.css'
import { SideBarDataProfessor } from './SideBarDataProfessor';
import { SideBarDataSeceretary } from './SideBarDataSeceretary';
import { SideBarDataStudent } from './SideBarDataStudent';

const MenuBar = () => {
  
  const sideBarItems = () => {
    if(currentUser.statut==='admin'){
      if(currentUser.fonction==='seceretary'){        
        return SideBarDataSeceretary.map((item, index) => {
          return (
            <li key={index} className={item.cName}>
              <Link to={currentUser.statut+"/"+currentUser.fonction+"/" +item.path}>
                {item.icon}
                <span>{item.title}</span>
              </Link>
            </li>
          );
        })
      }
      if(currentUser.fonction==='proffessor'){
        return SideBarDataProfessor.map((item, index) => {
          return (
            <li key={index} className={item.cName}>
              <Link to={currentUser.statut+"/"+currentUser.fonction+"/" +item.path}>
                {item.icon}
                <span>{item.title}</span>
              </Link>
            </li>
          );
        })
      }
    }
    if(currentUser.statut==='student'){
      return SideBarDataStudent.map((item, index) => {
        return (
          <li key={index} className={item.cName}>
            <Link to={currentUser.statut+"/"+currentUser.formation+"/" +item.path}>
              {item.icon}
              <span>{item.title}</span>
            </Link>
          </li>
        );
      })
    }
  }
  return (
    <div className='side-bar-menu col-12'>
      <div className='col-12'>
        {sideBarItems()}
      </div>  
    </div>
  );
};

export default MenuBar;