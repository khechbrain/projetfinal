import React from 'react';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import * as IoIcons from 'react-icons/io';
import * as GrIcons from 'react-icons/gr';
import * as GoIcons from 'react-icons/go';

export const SideBarDataProfessor = [
  {
    title: 'Accueil',
    path: '',
    icon: <AiIcons.AiFillHome />,
    cName: 'nav-text'
  },
  {
    title: 'Liste des étudiants',
    path: 'usersList',
    icon: <FaIcons.FaGraduationCap />,
    cName: 'nav-text'
  },
  {
    title: 'Liste des cours',
    path: 'cours',
    icon: <IoIcons.IoIosPaper />,
    cName: 'nav-text'
  },
  {
    title: 'Modules',
    path: 'modules',
    icon: <GoIcons.GoFileSubmodule />,
    cName: 'nav-text'
  },
  {
    title: 'Demande d\'absence',
    path: 'demande',
    icon: <IoIcons.IoMdMail />,
    cName: 'nav-text'
  }
];