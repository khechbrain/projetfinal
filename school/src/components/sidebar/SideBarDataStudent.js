import React from 'react';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import * as IoIcons from 'react-icons/io';
import * as HiIcons from 'react-icons/hi';

export const SideBarDataStudent = [
  {
    title: 'Accueil',
    path: '',
    icon: <AiIcons.AiFillHome />,
    cName: 'nav-text'
  },
  {
    title: 'Programme',
    path: 'cours',
    icon: <IoIcons.IoIosPaper />,
    cName: 'nav-text'
  },
  {
    title: 'Groupe',
    path: 'groupe',
    icon: <HiIcons.HiUserGroup />,
    cName: 'nav-text'
  },
  {
    title: 'Demande d\'absence',
    path: 'demande',
    icon: <IoIcons.IoMdMail />,
    cName: 'nav-text'
  }
];