import React from 'react';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import * as IoIcons from 'react-icons/io';
import * as GoIcons from 'react-icons/go';
import * as BsIcons from 'react-icons/bs';

export const SideBarDataSeceretary = [

  {
    title: 'Tableau de bord',
    path: '',
    icon: <AiIcons.AiFillHome />,
    cName: 'nav-text'
  },
  {
    title: 'Etudiants',
    path: 'studentsList',
    icon: <FaIcons.FaGraduationCap />,
    cName: 'nav-text'
  },
  {
    title: 'Cours',
    path: 'cours',
    icon: <IoIcons.IoIosPaper />,
    cName: 'nav-text'
  },
  {
    title: 'Modules',
    path: 'modules',
    icon: <GoIcons.GoFileSubmodule />,
    cName: 'nav-text'
  },
  {
    title: 'Professeurs',
    path: 'proflist',
    icon: <FaIcons.FaChalkboardTeacher />,
    cName: 'nav-text'
  },
  {
    title: 'Evenements',
    path: 'events',
    icon: <BsIcons.BsCalendarEvent />,
    cName: 'nav-text'
  },
  {
    title: 'Messages de diffusion',
    path: 'messages',
    icon: <AiIcons.AiFillMessage />,
    cName: 'nav-text'
  },
  {
    title: 'Demandes d\'absence',
    path: 'demandesList',
    icon: <IoIcons.IoMdMailOpen />,
    cName: 'nav-text'
  }
];