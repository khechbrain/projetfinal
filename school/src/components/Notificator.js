import React, { Component } from 'react';
import {MdNotifications} from "react-icons/md"
import {BsFillCircleFill} from "react-icons/bs"
import { collection, deleteDoc, doc, onSnapshot, setDoc } from 'firebase/firestore';
import { currentUser, currentUserId, firestore } from '../utils/firebase';

class Notificator extends Component {
    state={
        hasNotif:false,
        allMessages:[],
        user:null,
        showed:false
    }
    constructor(props) {
        super(props);
        
        // window.addEventListener("click",()=>{
        //     this.setState({showed:false})
        // })
    }
    componentDidMount() {
        onSnapshot(collection(firestore,'requests'),(snapshot)=>{
            snapshot.forEach((snap)=>{
                let demande = snap.data()
                if(demande.authorId===currentUserId){
                    if(demande.state==='yes'){
                        let newMsg ={
                            data(){
                                return  {title:'Votre demande d\'absence a été accepté',description:'du '+demande.debut.split("-").reverse() +" au "+demande.fin.split("-").reverse()}
                            }
                        }
                        this.setState({
                            hasNotif:!demande.isReaded,
                            allMessages: [...this.state.allMessages,newMsg]
                        })
                    }
                    if(demande.state==='no'){
                        let newMsg ={
                            data(){
                                return  {title:'Votre demande d\'absence a été refusé',description:'Vous êtes prié de d\'assister au cours'}
                            }
                        }
                        this.setState({
                            hasNotif:!demande.isReaded,
                            allMessages: [...this.state.allMessages,newMsg]
                        })

                    }
                }
            })
        })
        onSnapshot(collection(firestore,"messages"),(snapshot)=>{
            
            snapshot.forEach((snap)=>{
                if(currentUser.statut==="admin"){
                    if(snap.data().destination==="prof" ||snap.data().destination==="all"){
                        this.setState({allMessages:[...this.state.allMessages,snap]})
                    }
                }else{
                    if(snap.data().destination==="student" || snap.data().destination==="all"){
                        this.setState({allMessages:[...this.state.allMessages,snap]})
                    }
                }
            })
        })

    }

    render() {
        const allNotifications=()=>{
            if(this.state.allMessages.length===0){
                return <p>Pas notification pour le moment</p>
            }
            return this.state.allMessages.map((snap,index)=>{
                let message = snap.data()
                return (
                    <div key={index} className="text-start mx-2 fs-6">
                        <p className='fw-bold rounded'>{message.title}</p>
                        <p className='rounded'>{message.description}</p>
                        <hr></hr>
                    </div>
                )
            })
        }
        const reloadComponent=()=>{
            this.setState({allMessages:[]})

            onSnapshot(collection(firestore,'requests'),(snapshot)=>{
                snapshot.forEach((snap)=>{
                    let demande = snap.data()
                    if(demande.authorId===currentUserId){
                        if(demande.state==='yes'){
                            let newMsg ={
                                data(){
                                    return  {title:'Votre demande d\'absence a été accepté',description:'du '+demande.debut.split("-").reverse() +" au "+demande.fin.split("-").reverse()}
                                }
                            }
                            this.setState({
                                hasNotif:!demande.isReaded,
                                allMessages: [...this.state.allMessages,newMsg]
                            })
                        }
                        if(demande.state==='no'){
                            let newMsg ={
                                data(){
                                    return  {title:'Votre demande d\'absence a été refusé',description:'Vous êtes prié de d\'assister au cours'}
                                }
                            }
                            this.setState({
                                hasNotif:!demande.isReaded,
                                allMessages: [...this.state.allMessages,newMsg]
                            })
    
                        }
                    }
                })
            })
            onSnapshot(collection(firestore,"messages"),(snapshot)=>{
                
                snapshot.forEach((snap)=>{
                    if(currentUser.statut==="admin"){
                        if(snap.data().destination==="prof" ||snap.data().destination==="all"){
                            this.setState({allMessages:[...this.state.allMessages,snap]})
                        }
                    }else{
                        if(snap.data().destination==="student" || snap.data().destination==="all"){
                            this.setState({allMessages:[...this.state.allMessages,snap]})
                        }
                    }
                })
            })
        }
        const deleteDemendes=()=>{
            onSnapshot(collection(firestore,"requests"),(snapshot)=>{
                snapshot.forEach((snap)=>{
                        let demande = snap.data()
                        if(demande.authorId===currentUserId){
                            demande.isReaded = true
                            setDoc(doc(firestore,"requests",snap.id),demande)
                            .then(()=>{
                                reloadComponent()
                            })
                        }
                    }
                )
            })
        }
        const showAllNotifs =()=>{
            setTimeout(() => {
                this.setState({
                    hasNotif: false,
                    showed:!this.state.showed
                })
            }, 10);
            let readedNotifs=''

            this.state.allMessages.map((snap)=>{
                readedNotifs += snap.id+","
            })
            deleteDemendes()
            setDoc(doc(firestore,'notifications',this.state.user.id),{notifications:readedNotifs})
        }
        return (
            <>
                <button className='d-inline m-auto' onClick={showAllNotifs} style={{background:'rgba(0,0,0,0)',border:'none'}}>
                    <BsFillCircleFill className={this.state.hasNotif?'d-inline':'d-none'} style={{position:'absolute',color:'red',width:'10px',height:'10px',marginLeft:'15px',marginTop:'5px'}}></BsFillCircleFill>
                    <MdNotifications className='m-auto' style={{width:'30px',height:'30px',color:'#006bbd'}}></MdNotifications>
                </button>
                <div className={this.state.showed?'d-inline shadow':'d-none'} style={{background:'#fff',transition:'1000ms',position:'absolute',width :'25vw',right:'0',top:'75px',borderRadius:'0px 0px 10px 10px'}}>
                    {allNotifications()}
                </div>
            </>
        );
    }
}

export default Notificator;