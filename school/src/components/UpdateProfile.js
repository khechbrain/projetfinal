import { addDoc, collection, doc, onSnapshot, setDoc } from 'firebase/firestore';
import { getStorage, ref, uploadBytesResumable } from 'firebase/storage';
import React, { Component } from 'react';
import { app, currentUser, currentUserId, firestore } from '../utils/firebase';

class UpdateProfile extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            user : null
        }
    }
    componentDidMount() {
        // onSnapshot(doc(firestore,"users",this.props.userId),(snap)=>{
        //     let currentUser = snap.data()
            this.setState({
                fName: currentUser.fName,
                lName: currentUser.lName,
                number: currentUser.number,
                email: currentUser.email,
                imageUrl: currentUser.imageUrl,
                password: currentUser.password,
                comfirmPassword: currentUser.password,
                statut: currentUser.statut,
                isUpdated: false,
                isAdmin: currentUser.statut==='admin'? (true):(false),
                fDuration: currentUser.statut==='admin'? (""):(currentUser.fDuration),
                fonction: currentUser.statut==='admin'? (currentUser.fonction):(''),
                salary: currentUser.statut==='admin'?(currentUser.salary):(''),
                formation: currentUser.statut==='admin'? (''):(currentUser.formation),
                userImg:null
            })
        // },(error)=>alert(error))
    }
    
    

    render(){

        const randomNumber = Math.round(Math.random()*10000)
        const verification=()=>{
            return true
        }
        const updateUser =(e)=>{
            e.preventDefault(); // Stop the form from submitting
            let user = this.state.isAdmin? ({
                statut:this.state.statut,
                fName:this.state.fName,
                lName:this.state.lName,
                displayName:this.state.fName+' '+this.state.lName,
                number:this.state.number,
                email:this.state.email,
                fonction:this.state.fonction,
                imageUrl: this.state.imageUrl,
                salary:this.state.salary,
                password:this.state.password
            })
            :
            ({
                statut:this.state.statut,
                fName:this.state.fName,
                lName:this.state.lName,
                displayName:this.state.fName+' '+this.state.lName,
                number:this.state.number,
                email:this.state.email,
                formation:this.state.formation,
                fDuration:this.state.fDuration,
                imageUrl: this.state.imageUrl,
                password:this.state.password
            })

            let id = currentUserId
            if(verification()){
                if(this.state.userImg===null){
                    setDoc(doc(firestore,'users',id),user)
                    .then((result)=>{
                        window.history.back()
                    },(error)=>alert(error))
                }else{
                    user.imageUrl= 'https://firebasestorage.googleapis.com/v0/b/projet-final-bakeli.appspot.com/o/users%2Fprofiles%2F'+randomNumber+this.state.userImg.name+'?alt=media'
                    var storageRef = ref(getStorage(app),"/users/profiles/"+randomNumber+this.state.userImg.name)
                    uploadBytesResumable(storageRef,this.state.userImg)
                    .on("state_changed",()=>{
                    })
                    setTimeout(() => {
                        setDoc(doc(firestore,'users',id),user)
                        .then((result)=>{
                            window.history.back()
                        },(error)=>alert(error))
                    }, 2000);
                }
            }
        }
         
        return (
            <div className='mx-5 my-3'>
                <h3 className='titre-theme'>Modification du profile de l'uttilisateur:</h3>
                <div className='p-2 shadow' style={{border:'4px solid #2dcdcf',borderRadius:'10px'}}>
                    <form onSubmit={e=>updateUser(e)}>
                        <div className='row  d-flex justify-content-center align-items-center'>
                            <div className='col-12 col-sm-4 d-flex justify-content-center align-items-center'>
                                <div className='d-flex justify-content-center align-items-center' style={{width:'150px',height:'150px',background:'#d7e6f5',borderRadius:'10px',overflow:'hidden'}}>
                                    <img src={currentUser.imageUrl} style={{width:'100%'}}></img>
                                </div>
                            </div>
                            <div className='col-12 col-sm-4'>
                                <h4>Vous voulez changer votre photo de profil? Cliquer ici</h4>
                                <input type="file" className=' form-control' onChange={e=>this.setState({userImg:e.target.files[0]})}></input>
                            </div>
                        </div>
                        <hr></hr>
                        <div className='row justify-content-center my-1'>
                            <div className='col-12 col-sm-6 justify-content-start'>
                                <div className='input-group col-6'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Prénom:</label>
                                    <input type="text" required className='col-9 my-2 rounded form-control' placeholder='Prénom' value={this.state.fName} onChange={e=>this.setState({fName:e.target.value})}></input>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 '>
                                <div className='input-group col-6'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Nom:</label>
                                    <input type="text" required className='col-9 my-2 rounded form-control' placeholder='Nom' value={this.state.lName} onChange={e=>this.setState({lName:e.target.value})}></input>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 '>
                                <div className='input-group col-6'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Numéro:</label>
                                    <input type="number" required className='col-9 my-2 rounded form-control' placeholder='Numéro de téléphone' value={this.state.number} onChange={e=>this.setState({number:e.target.value})}></input>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 '>
                                <div className='input-group col-6'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Email:</label>
                                    <input disabled type="email" required className='col-9 my-2 rounded form-control' placeholder='Email' value={this.state.email} onChange={e=>this.setState({email:e.target.value})}></input>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 col-md-9 col-lg-6 '>
                                <div className='input-group col-6'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Mot de passe:</label>
                                    <input type="password" required className='col-9 my-2 rounded form-control' placeholder='Mot de passe' value={this.state.password} onChange={e=>this.setState({password:e.target.value})}></input>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 col-md-9 col-lg-6 '>
                                <div className='input-group col-6'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Confirmer Mot de passe:</label>
                                    <input type="password" required className='col-9 my-2 rounded form-control' placeholder='Mot de passe' value={this.state.comfirmPassword} onChange={e=>this.setState({comfirmPassword:e.target.value})}></input>
                                </div>
                            </div>
                        </div>
                        <button type='submit' className='btn col-10 col-sm-6 col-md-8 col-lg-6  btn-dark my-2' style={{zIndex:'1'}}>Mettre a jour mon profile</button>
                    </form>
                </div>
            </div>
        );
    }
}

export default UpdateProfile;