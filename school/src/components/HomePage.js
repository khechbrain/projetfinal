import React, { useEffect } from 'react';
import { Route, Routes, useNavigate } from 'react-router-dom';
import NavBarComponent from './NavBarComponent';
import AdminPage from './admin/AdminPage';
import {app,currentUser} from '../utils/firebase'
import StudentPage from './student/StudentPage';
import { getDatabase,get,ref, onValue } from 'firebase/database';
import MenuBar from './sidebar/MenuBar';
import image from '../img/bg_home.png';
import UpdateProfile from './UpdateProfile';
import Reglement from './Reglement'

const HomePage = () => {
 let navigate = useNavigate()
    
    // Cette partie cest pour verifier la connexion internet
    // Ne modifiez pas cette page
    useEffect(()=>{
        onValue(ref(getDatabase(app),"test"),(snapshot)=>{
            if(currentUser.statut==='admin'){
                navigate('admin')
                console.log(currentUser);
            }else if(currentUser.statut==='student'){
                navigate('student')
                console.log(currentUser);
            }
        },(error)=>{
            alert('Echec du chargement veuillez verifier votre connexion internet')
        })
    },[])
    // ######################################################//

    return (
        <div className='row'>
            <NavBarComponent></NavBarComponent>
            
            <div className='row page-body mx-0 px-0'>
              <div className='col-4 col-md-3 col-lg-2 mx-0 px-0 d-none d-md-flex' style={{background:'#006bbd',height:'100vh'}}>
                  <MenuBar></MenuBar>
              </div>
              <div className='col-12 col-md-9 col-lg-10 mx-0 px-0 mx-auto px-auto justify-content-center'
                // style={{backgroundImage:'url('+image+')',backgroundSize:'contain'}} 
                style={{background:"#ebf2f6"}}
                >

                    <Routes>
                        <Route path="admin/*" element={<AdminPage></AdminPage> }></Route>
                        <Route path="student/*" element={<StudentPage></StudentPage> }></Route>
                        <Route path="updateProfile/*" element={<UpdateProfile></UpdateProfile> }></Route>
                        <Route path="reglement/*" element={<Reglement></Reglement> }></Route>
                    </Routes>
              </div>
            </div>
        </div>
    );
};

export default HomePage;