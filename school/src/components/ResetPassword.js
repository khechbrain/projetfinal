import { getAuth, RecaptchaVerifier, signInWithPhoneNumber } from 'firebase/auth';
import { collection, doc, onSnapshot, setDoc } from 'firebase/firestore';
import React, { Component } from 'react';
import {BsFillEyeFill} from 'react-icons/bs'
import {BsFillEyeSlashFill} from 'react-icons/bs'
import {BsEnvelope,BsFillTelephoneFill} from 'react-icons/bs'
import {RiLockPasswordFill} from 'react-icons/ri'
import {FiLock} from 'react-icons/fi'
import { Link } from 'react-router-dom';
import logo from '../img/logo.png'
import { app, firestore } from '../utils/firebase';

class ResetPassword extends Component {
    state={
        email:'',
        number:'',
        otp:'',
        password:'',
        confirmPassword:'',
        finalOTP:null,
        allUsersSnap:[],
        userNumber:'',
        userEmail:'',
        currentUserSnap:null,
        showInputEmail:true,
        showInputNumber:false,
        showInputOTP:false,
        showInputNewPassword:false,
        showLastMessage:false,
        hasError:false,
        errorMsg:""
    }
    componentDidMount() {
        onSnapshot(collection(firestore,'users'),(snapshot)=>{
            snapshot.forEach((snap)=>{
                this.setState({allUsersSnap:[...this.state.allUsersSnap,snap]})
            })
        })
    }
    
    render() {
        const comfirmEmail =()=>{
            let found = false
            this.state.allUsersSnap.map((snap)=>{
                let user = snap.data()
                if(this.state.email===user.email){
                    found = true
                    this.setState({
                        currentUserSnap:snap,
                        showInputEmail:false,
                        showInputNumber:true
                    })
                }
            })
            if(!found){
                this.setState({
                    hasError:true,
                    errorMsg:'Adresse email introuvable'
                })
            }
        }
        const comfirmNumber =()=>{
            let userNumber = this.state.currentUserSnap.data().number
            if(this.state.number===userNumber){
                const verifier = new RecaptchaVerifier('container-recaptcha',{},getAuth(app))
                signInWithPhoneNumber(getAuth(app),"+221"+userNumber,verifier)
                .then((result)=>{
                    this.setState({
                        showInputNumber:false,
                        finalOTP:result,
                        showInputOTP:true
                    })
                })
            }else{
                this.setState({
                    hasError:true,
                    errorMsg:'Numéro de téléphone incorrect'
                })
            }
        }
        const comfirmOTP =()=>{
            this.state.finalOTP.confirm(this.state.otp).then((result)=>{
                {this.setState({
                    showInputOTP:false,
                    showInputNewPassword:true
                })}
            }).catch(()=>{
                this.setState({
                    hasError:true,
                    errorMsg:'Code de confirmation incorrect'
                })
            })
        }
        const createPassword=()=>{
            if(this.state.password===this.state.confirmPassword){
                if(this.state.password.length>=6){
                    let newUser = this.state.currentUserSnap.data()
                    newUser.password = this.state.password
                    setDoc(doc(firestore,"users",this.state.currentUserSnap.id),newUser)
                    .then(()=>{
                        this.setState({
                            showInputNewPassword:false,
                            showLastMessage:true
                        })
                    })
                }else{
                    this.setState({
                        hasError:true,
                        errorMsg:'Le mot de passe doit contenir au minimum 6 caractères'
                    })
                }
            }else{
                this.setState({
                    hasError:true,
                    errorMsg:'Les deux mots de passe ne sont pas identiques'
                })
            }
        }
        return (
            <div className='row bg-light' style={{height:'100vh'}} > 
                <div className='col-6 log_img d-none d-md-block' >
                   <div style={{marginTop:'35%'}}>
                       <h1 className='text-light' style={{textShadow:'0 0 5px #006bbd',textAlign:'center',fontSize:'400%',fontFamily:'poppins'}}>Daradji Tech</h1>
                       <h3 className='text-light' style={{textShadow:'0 0 5px #006bbd',textAlign:'center',fontFamily:'poppins'}}>Ecole de formation 100% pratique</h3>
                   </div>   
                </div> 
                <div className=' col-12 col-md-6 d-block row justify-content-center my-5'>
                
                    <div className={this.state.hasError?'d-flex bg-warning row m-1 rounded':'d-none'}>
                        <p className='col-11'>{this.state.errorMsg}</p>
                        <button type='button' className='col-1 btn-danger rounded' onClick={()=>this.setState({hasError:false})}>X</button>
                    </div>

                    <div className='row d-flex'>
                        <div className='d-inline row mx-auto' style={{width:'200px'}}>
                            <img src={logo}/>
                        </div>
                    </div>
                    <div className={this.state.showInputEmail?'row mx-auto':'d-none'}>
                            <h2>Récuperation de mot de passe:</h2>
                            <p>Veuillez saisir votre adresse email</p>

                            <div className='input-group shadow rounded'>
                              <span className='input-group-text ' style={{marginLeft:'0',height:'38px',marginTop:'8px',color:'blue'}}> <BsEnvelope /> </span>
                              <input type="email" required className='my-2 form-control' placeholder='Email' value={this.state.email} onChange={e=>this.setState({email:e.target.value})}></input>
                            </div>    
                        <button onClick={comfirmEmail} className='btn btn-dark col-10 col-sm-8 col-md-10 col-lg-6 my-4 m-auto'>Confirmer l'adresse email</button>
                    </div>
                    <div className={this.state.showInputNumber?'row mx-auto':'d-none'}>
                            <h2>Récuperation de mot de passe:</h2>
                            <p>Veuillez saisir votre numéro de téléphone : ** *** ** {this.state.currentUserSnap===null?"":this.state.currentUserSnap.data().number.substring(7,9)} </p>

                            <div className='input-group shadow rounded'>
                              <span className='input-group-text ' style={{marginLeft:'0',height:'38px',marginTop:'8px',color:'blue'}}> <BsFillTelephoneFill /> </span>
                              <input type="number" required className='my-2 form-control' placeholder='Numéro de téléphone' value={this.state.number} onChange={e=>this.setState({number:e.target.value})}></input>
                            </div>                        
                            <div id='container-recaptcha'></div>    
                        <button onClick={comfirmNumber} className='btn btn-dark col-10 col-sm-8 col-md-10 col-lg-6 my-4 m-auto'>Confirmer mon numéro de téléphone</button>
                    </div>
                    <div className={this.state.showInputOTP?'row mx-auto':'d-none'}>
                            <h2>Récuperation de mot de passe:</h2>
                            <p>Veuillez saisir le code de validation (OTP) qui vous a été envoyé par SMS au {this.state.currentUserSnap===null?"":this.state.currentUserSnap.data().number} </p>

                            <div className='input-group shadow rounded'>
                              <span className='input-group-text ' style={{marginLeft:'0',height:'38px',marginTop:'8px',color:'blue'}}> <RiLockPasswordFill /> </span>
                              <input type="number" required className='my-2 form-control' placeholder='Code de confirmation' value={this.state.otp} onChange={e=>this.setState({otp:e.target.value})}></input>
                            </div> 
                        <button onClick={comfirmOTP} className='btn btn-dark col-10 col-sm-8 col-md-10 col-lg-6 my-4 m-auto'>Confirmer le code de validation</button>
                    </div>
                    <div className={this.state.showInputNewPassword?'row mx-auto':'d-none'}>
                            <h2>Rénitialisation du mot de passe :</h2>
                            <p>Veuillez créer un nouveau mot de passe de 6 charactéres au minimum</p>

                            <div className='input-group shadow rounded'>
                              <span className='input-group-text ' style={{marginLeft:'0',height:'38px',marginTop:'8px',color:'blue'}}> <RiLockPasswordFill /> </span>
                              <input type="password" required className='my-2 form-control' placeholder='Mot de passe' value={this.state.password} onChange={e=>this.setState({password:e.target.value})}></input>
                            </div> 
                            <div className='input-group shadow rounded'>
                              <span className='input-group-text ' style={{marginLeft:'0',height:'38px',marginTop:'8px',color:'blue'}}> <RiLockPasswordFill /> </span>
                              <input type="password" required className='my-2 form-control' placeholder='Confirmer Mot de passe' value={this.state.confirmPassword} onChange={e=>this.setState({confirmPassword:e.target.value})}></input>
                            </div> 
                        <button onClick={createPassword} className='btn btn-dark col-10 col-sm-8 col-md-10 col-lg-6 my-4 m-auto'>Confirmer le nouveau mot de passe</button>
                    </div>
                    <div className={this.state.showLastMessage?'row mx-auto':'d-none'}>
                            <h2>Réinitialisation du mot de passe réuissi</h2>
                            <p>Votre mot de passe a bien été reinitialisé</p>
                    </div>
                <Link to="/login">Retour</Link>
                </div>
            </div>
        );
    }
}

export default ResetPassword;