import { getDatabase, onValue, ref } from 'firebase/database';
import React, { useEffect } from 'react';
import { Route, Routes, useNavigate } from 'react-router-dom';
import { app, currentUser } from '../../utils/firebase';

import StudentHome from './StudentHome'
import StudentCoursList from './StudentCoursList'
import DemandeStudent from './DemandeStudent';
import GroupeStudents from './GroupeStudents';

const StudentPage = () => {
 let navigate = useNavigate()

    // Cette partie cest pour verifier la connexion internet
    // et les infos de l'uillisateur
    // Ne modifiez pas cette page
    useEffect(()=>{
        onValue(ref(getDatabase(app),"test"),(snapshot)=>{
            if(currentUser.formation===''){
                navigate('/')
                console.log(currentUser);
            }else{
                navigate(currentUser.formation)
                console.log(currentUser);
            }
        },(error)=>{
            alert('Echec du chargement veuillez verifier votre connexion internet')
        })
    },[])
    // ######################################################//

    return (
        <div className='row'>
            <div className='col-12'>
                <Routes>
                    <Route path={currentUser.formation} element={<StudentHome/> }></Route> 
                    <Route path={currentUser.formation+"/"+"cours"} element={<StudentCoursList/> }></Route> 
                    <Route path={currentUser.formation+"/"+"demande"} element={<DemandeStudent/> }></Route> 
                    <Route path={currentUser.formation+"/"+"groupe"} element={<GroupeStudents/> }></Route> 
                </Routes>
            </div>
        </div>
    );
};

export default StudentPage;