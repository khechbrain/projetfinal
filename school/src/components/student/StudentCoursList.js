import React from 'react';
import {onSnapshot, collection} from "firebase/firestore"
import {currentUser, firestore} from "../../utils/firebase"
import { Component } from 'react';

export default class StudentCoursList extends Component{
  state={
    allModules :[],
    allCours:[],
    currentModule:'programation',
    currentModuleRoute:'non définie a l\'origine'
  }
  constructor(props) {
      super(props);
      
      onSnapshot(collection(firestore,"modules"),(snapshot)=>{
          snapshot.forEach((snap)=>{
              let module = snap.data()
              if(module.domain === currentUser.formation){
                this.setState(
                    {allModules: this.state.allModules.concat([snap])}
                )
              }
          })
      })
      onSnapshot(collection(firestore,"cours"),(snapshot)=>{
          snapshot.forEach((snap)=>{
            let cours = snap.data()
            if(cours.domain===currentUser.formation){
                this.setState(
                    {allCours: this.state.allCours.concat([snap])}
                )
            }
          })
      })
      
  }
  render(){
    const allCoursList =()=>{
        if(this.state.allCours.length===0){
            return <h2 className='text-dark'>Aucun cours n'a été encore ajouté ici !!!</h2>
        }
        return this.state.allCours.map((snap,index)=>{
            let lesson = snap.data()
            // if(lesson.module === this.props.currentModuleRoute){ 
                return (
                    <div key={index} className="card my-2 mx-auto">
                        <div className="card-body text-start">
                            <blockquote className="blockquote mb-0">
                            <div className='row'>
                                <p className='col-12 col-md-6 col-lg-4 me-auto fs-6 text-secondary text-start'>Cours: {index+1} </p>
                                <p className='col-12 col-md-6 col-lg-4 ms-lg-auto fs-6 text-secondary text-lg-end'>Durée: {lesson.duration} </p>
                            </div>
                            <p className='bg-light fw-bold rounded text-center'>{lesson.name} </p>
                            <p className='fs-6 text-center'>{lesson.description}</p>
                            <p className='fs-6 justify-content-center d-flex'>
                                <a className='col-6' href={lesson.link} target='_blank'>
                                    <button className='btn btn-dark col-12 m-auto'>Ouvrir le lien</button>
                                </a>
                            </p>
                            {/* <p className=' fs-6'>
                                <a href={lesson.link} target='_blank'>{lesson.link}</a>
                            </p> */}
                            </blockquote>
                        </div>
                    </div>
                )
            // }
        })
    }
    const showCoursList =(moduleTitle)=>{
        this.setState({allCours:[]})
        onSnapshot(collection(firestore,"cours"),(snapshot)=>{
            snapshot.forEach((snap)=>{
                let cours = snap.data()
                if(cours.module === moduleTitle){
                  this.setState(
                      {allCours: this.state.allCours.concat([snap])}
                  )
                }
            })
        })
    }
    const allModulesList =()=>{
        return this.state.allModules.map((snap,index)=>{
            let module = snap.data()
            
                if(index === this.state.allModules.length-1 && this.state.allModules===[]){
                    return <h1>aucun module n'a été ajouté por ce domaine</h1>
                }
                return (  
                    <div key={index} className="col-12 col-sm-6 col-lg-12 card my-2 py-2" style={{maxHeight:'250px'}}>
                        <img src={module.imageUrl} style={{height:100}} className="card-img-top image" alt="Skyscrapers"/>
                        <div className="card-body py-2">
                        
                        <h5 className="card-title">{module.title} </h5>
                        </div>
                        <div className="card-footer">
                            <button className="btn btn-primary" 
                            onClick={e=>showCoursList(module.title)} >Voir cours</button>
                        </div>
                    </div>
                )
            })
    }
    return (
      <div className="row m-3 justify-content-center" style={{borderRadius:"30px"}}>
      
          <div className="row col-12 py-3 mx-auto justify-content-center">
            <div className='row'>
                <div className='col-12 col-lg-8 mx-auto justify-content-center'>
                    <h3 className='titre-theme'>Liste des cours</h3>
                    <div className='row p-3 shadow scroll-theme' style={{borderRadius:"30px",height:'70vh',overflowY:'scroll',overflowX: 'hidden'}}>
                        {allCoursList()}
                    </div>
                </div>
                <div className='col-lg-4 justify-content-center'>
                    <h3 className='titre-theme'>Modules</h3>
                    <div className='row mx-auto justify-content-center scroll-theme' style={{borderRadius:"30px",height:'70vh',overflowY:'scroll',overflowX: 'hidden'}}>
                        {allModulesList()}
                    </div>
                </div>
            </div>
         
      </div>
      </div>
  
  
    );
  }

 
}
