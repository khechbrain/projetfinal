import React, {useState,useEffect} from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import moment from 'moment';
const date = new Date();

export default function Calendardesign() {
  const [dateTime, setDateTime] = useState({
    hours: date.getHours(),
    minutes: date.getMinutes(),
    seconds: date.getSeconds()
  });

  
  
  useEffect(() => {
    const timer = setInterval(() => {
      const date = new Date();
      setDateTime({
        hours:  date.getHours()<10? ("0"+date.getHours()):(date.getHours()),
        minutes:  date.getMinutes()<10? ("0"+date.getMinutes()):(date.getMinutes()),
        seconds: date.getSeconds()<10? ("0"+date.getSeconds()):(date.getSeconds())
      });
     
    }, 1000);
    return () => clearInterval(timer);
  }, []);
  
  const [dateState, setDateState] = useState(new Date())
  const changeDate = (e) => {
    setDateState(e)
  }
  return (
    <div  className='col mx-5  ' style={{marginTop:"25%"}}  >
           <div  style={{backgroundColor:"white",width:"98%",height:"100px"  ,marginTop:"1%"}}>
      <div  style={{color:"blue",fontWeight:"bolder",fontSize:"400%",textTransform:"uppercase" ,paddingTop:"5%"}}>
        {dateTime.hours}:{dateTime.minutes}:{dateTime.seconds}
      </div>

      
        
      </div>
      <div style={{marginTop:"10%"}}>
      <Calendar  
      value={dateState}
      onChange={changeDate}
      />

      </div>
     
     
      <div style={{backgroundColor:"white",width:"100%",height:"60vh"  ,marginTop:"10%"}} > 
        <div  style={{color:"blue",fontWeight:"bolder",fontSize:"200px",textTransform:"uppercase" ,paddingTop:"10%" }}>
          <h1>Horaire des etudiants</h1>
        </div>
        <div style={{color:"black",fontWeight:"bolder",fontSize:"100%",textTransform:"uppercase" ,paddingbottom:"10px" }} >
          <p>lundi:08h-12h</p>
          <p>mardi:08h-12h</p>
          <p>mercredi:08h-12h</p>
          <p>jeudi:08h-12h</p>
          <p>vendredi:08h-12h</p>
        </div>
        
      </div>

    
    </div>
  )
}


