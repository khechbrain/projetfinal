import { collection, onSnapshot } from 'firebase/firestore';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { currentUser, firestore } from '../../utils/firebase';
import Calendrier from '../admin/seceretary/Calendrier';


class StudentHome extends Component{

    state={
        profList:[],
        modulesList:[]
    }
    componentDidMount() {
        
        onSnapshot(collection(firestore,"users"),(snapshot)=>{
            snapshot.forEach((snap)=>{
                let user = snap.data()
                if(user.statut==='admin' && user.fonction === 'proffessor'){
                    this.setState({profList:[...this.state.profList,user]})
                }
            })
        })
        onSnapshot(collection(firestore,"modules"),(snapshot)=>{
            snapshot.forEach((snap)=>{
                let module = snap.data()
                if(module.domain===currentUser.formation){
                    this.setState({modulesList:[...this.state.modulesList,module]})
                }
            })
        })
    }
    
    render(){

        const listProfesseurs=()=>{
            return this.state.profList.map((user)=>{
                if(user.statut==="admin"){
                    return (
                        <div className='card col-6 col-sm-4 col-md-3 col-lg-2 row m-4' style={{borderRadius:'15px'}}>
                            <div className='col-10 m-auto m-0 p-2 d-flex align-items-center justify-content-center'>
                                <div style={{background:'#d7e6f5',width:'100%',maxWidth:'150px',height:"150px",overflow:'hidden', display:'flex',borderRadius:'7px'}}>
                                    <img src={user.imageUrl} className='m-auto rounded shadow' style={{width:'100%',margin:'0',}}></img>
                                </div>  
                            </div>
                            <p className='fw-bold fs-5' style={{color:''}}>{user.displayName} </p>
                        </div>
                    )
                }
            })
        }
        const listModules=()=>{
            return this.state.modulesList.map((module)=>{
                return (
                    <div className='card col-6 col-sm-4 col-md-3 col-lg-2 row m-4' style={{borderRadius:'15px'}}>
                        <div className='col-10 m-auto m-0 p-2 d-flex align-items-center justify-content-center'>
                            <div style={{background:'#d7e6f5',width:'100%',maxWidth:'150px',height:"100px",overflow:'hidden', display:'flex',borderRadius:'7px'}}>
                                <img src={module.imageUrl} className='m-auto rounded shadow' style={{width:'100%',margin:'0',}}></img>
                            </div>  
                        </div>
                        <p className='fw-bold fs-5' style={{color:''}}>{module.title.toUpperCase()} </p>
                        <Link to="cours">
                            <button className='btn btn-dark mb-2 col-8 col-md-10 m-auto'>Voir cours</button>
                        </Link>
                    </div>
                )
            })
        }
        return (
            <div className='row justify-content-center'>
                <div className='my-2 align-items-center'>
                    <h2 className='titre-theme d-inline px-3'>Bienvenue</h2>
                </div>
                <div className='col-12'>
                    <p className='fw-bold fs-4' style={{color:'#2dcdcf'}}>Nos professeurs:</p>
                    <div className='row justify-content-center' style={{background:'#2dcdcf'}}>
                        {listProfesseurs()}
                    </div>
                    <p className='fw-bold fs-4' style={{color:'#2dcdcf'}}>Liste des modules:</p>
                    <div className='row justify-content-center' style={{background:'#2dcdcf'}}>
                        {listModules()}
                    </div>
                    <div className='row bg-light my-3 mx-3 me-md-4 m-auto'>
                        <Calendrier></Calendrier>
                    </div>
                </div>
            </div>
    
        );
    }
};

export default StudentHome;