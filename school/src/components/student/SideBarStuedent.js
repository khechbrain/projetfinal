import React from 'react';
import { Link } from 'react-router-dom';
import  'material-icons/iconfont/material-icons.css' ;

const SlideBarSeceretary = () => {

    const showSidebar = () =>{
       var menu = document.getElementById("sidebar");
       for(let i =300; i>0; i--){
        setTimeout(() => {
            menu.style.left = "-" + i + "px";
        }, 1000);
       }
        // menu.style.marginLeft=0
    }
    const hideSidebar = () =>{
        var menu = document.getElementById("sidebar");
       for(let i = 0; i<300; i++){
        setTimeout(() => {
            menu.style.left = "-" + i + "px";
        }, 1000);
       }
    }

    return (
        <div className='menu row'>
        <ul className='col-3 bg-secondary' style={{zIndex:'1'}} onMouseOver={showSidebar} onMouseOut={hideSidebar}>
            <Link to="" className='text-decoration-none'>
                <p className='col-12 bg-dark my-2  text-light rounded'>
                <span className="material-icons-outlined icons">
                grid_view
                </span></p>
            </Link>
            <Link to="usersList" className='text-decoration-none'>
                <p className='col-12 bg-dark my-2  text-light rounded'>
                <span className="material-icons-outlined icons">
                school
                </span></p>
            </Link>
            <Link to="cours" className='text-decoration-none'>
                <p className='col-12 bg-dark my-2  text-light rounded'>
                <span className="material-icons-outlined">
                library_books
                </span></p>
                    </Link>
            <Link to="modules" className='text-decoration-none'>
                <p className='col-12 bg-dark my-2  text-light rounded'>
                <span className="material-icons-outlined">
                view_module
                </span>
                </p>
            </Link>

            <Link to="proflist" className='text-decoration-none'>
                <p className='col-12 bg-dark my-2  text-light rounded'>
                <span className="material-icons-outlined">
                perm_identity
                </span></p>
            </Link>
        </ul>
            <ul className='col-8 bg-secondary sidebar text-center ms-5' id='sidebar'>
                <Link to="" className='text-decoration-none'>
                    <p className='col-12 bg-dark my-2 sidebar text-light rounded'>
                    &nbsp;  &nbsp; &nbsp;Tableau de bord</p>
                </Link>
                <Link to="usersList" className='text-decoration-none'>
                    <p className='col-12 bg-dark my-2  text-light rounded'>
                      Etudiants</p>
                </Link>
                <Link to="cours" className='text-decoration-none'>
                    <p className='col-12 bg-dark my-2  text-light rounded'>
                    Cours</p>
                        </Link>
                <Link to="modules" className='text-decoration-none'>
                    <p className='col-12 bg-dark my-2  text-light rounded'>
                    Modules
                    </p>
                </Link>

                <Link to="proflist" className='text-decoration-none'>
                    <p className='col-12 bg-dark my-2  text-light rounded'>
                    Proffessor</p>
                </Link>
            </ul>
        </div>
    );
};

export default SlideBarSeceretary;