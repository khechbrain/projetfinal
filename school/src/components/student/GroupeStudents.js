import { collection, onSnapshot } from 'firebase/firestore';
import React, { Component } from 'react';
import { firestore } from '../../utils/firebase';

class GroupeStudents extends Component {
    state={
        groupeList:[],
    }
    componentDidMount() {
        onSnapshot(collection(firestore,"users"),(snapshot)=>{
            snapshot.forEach((snap)=>{
                let user = snap.data()
                if(user.statut==='student'){
                    this.setState({groupeList:[...this.state.groupeList,user]})
                }
            })                
        })
    }
    
    render() {
        const allStudents =()=>{
            return this.state.groupeList.map((user)=>{
                return (
                    <li className='row m-0 p-0 overflow-hidden my-1 bg-light border rounded'>
                        <span className='col-5 col-md-3'>{user.fName} </span>
                        <span className='col-5 col-md-3'>{user.lName} </span>
                        <span className='col-5 col-md-3'>{user.number} </span>
                        <span className='col-5 col-md-2'>{user.email} </span>
                    </li>
                )
            })
        }
        return (
            <div className='row m-3 me-md-4'>
                <h3 className='titre-theme'>Groupe</h3>
                <div className='row m-0 p-0 m-auto'>
                    <ul className='text-start'>
                    
                        <li className='row m-0 p-0 fw-bold bg-light border border-secondary'>
                            <span className='col-5 col-md-3'>Prénom </span>
                            <span className='col-5 col-md-3'>Nom </span>
                            <span className='col-5 col-md-3'>Numéro </span>
                            <span className='col-5 col-md-2'>Email </span>
                        </li>
                      {allStudents()}
                    </ul>
                </div>
            </div>
        );
    }
}

export default GroupeStudents;