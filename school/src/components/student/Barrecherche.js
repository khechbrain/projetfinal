import React from 'react';

function Barrecherche() {
  return (
      <div >
      <form className="d-flex mt-5">
        <input className="form-control me-2 " type="search" placeholder="Search" aria-label="Search" style={{width:'35%',marginLeft:'30%'}}/ >
        <button className="btn btn-outline-success" type="submit">Search</button>
      </form>
      </div>
  )
}

export default Barrecherche;
