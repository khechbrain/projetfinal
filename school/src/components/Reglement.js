import React,{useState} from 'react';
import "./Reglement.css"

const Reglement = () => {
    const [handletoggler,sethandletoggler] = useState(true)
    return (
 <div className='text-start ms-3 row'>
     <h2 className='text-center titre-theme'>REGLEMENT INTERIEUR</h2>

 <div className='row fs-5'>  
<p>Les étudiants doivent également se rapprocher de leurs coachs après chaque retard et se justifier.
Aucun étudiant ne peut s'absenter de son poste de travail sans motif valable, ni quitter l'établissement sans autorisation préalable.</p> 
<h6 className=' fs-5'>Article 1: Exécution du travail</h6>
<p>Les étudiants doivent se conformer aux directives qui leurs sont données par leurs coachs et effectuer leurs tâches dans les meilleurs délais en vue de faciliter la formation.
Tout manquement

Sont susceptibles d'être mises en oeuvre dans l'entreprise, les sanctions suivantes :

Paiement d’une amende de 50 FCFA pour chaque minute de retard
Pompe gymnastique pour chaque minute de retard
Avertissement oral
Avertissement écrit
Renvoi de la formation sans préavis
L’étudiant sera convoqué par l'employeur à un entretien préalable lorsque celui-ci envisagera de prendre une des 3 dernières sanctions qui précède à son 
égard.</p><br></br>
<button onClick={e=>sethandletoggler(!handletoggler)} className="btn  btn-dark m-auto col-4 col-md-3 col-lg-2">INTEGRALITE</button>
</div>
<div className={handletoggler?('d-none'):('d-block fs-5')} >

<h6 className=' fs-5'>Article 2: Tabac</h6>
<p>Il est interdit de fumer dans tous les lieux fermés de Daradji Academy.
Il est donc formellement interdit de fumer dans : les bureaux, la salle de détente et de repas, la salle de réunion, les couloirs.
Sauf dans le lieu spécialement réservé aux fumeurs.</p>

<h6 className=' fs-5'>Article 3: Boissons et restaurations</h6>
<p>Les boissons et tout autre aliment devront être consommés dans la cuisine.
La salle de détente et de repas est un lieu convivial, commun qui doit rester propre et bien tenu à tout instant.</p>

<h6 className=' fs-5'>Article 4: Règles d’hygiène</h6>
<p>Les étudiants veilleront à ce que:
Les ustensiles de cuisine soient nettoyés après chaque utilisation.
De passer la serpillère après les ablutions.
Maintenir les toilettes propres.
De veiller à la propreté de son espace de travail.</p>

<h6 className=' fs-5'>Article 5: Autres dispositions</h6>
<p>Toute personne ayant accès aux locaux ne peut :
Causer du désordre en quelque lieu que ce soit
Faire du bruit dans les couloirs, salle de travail, salle de détente de manière à nuire au bon déroulement des activités de la société.</p>

<h6 className=' fs-5'>Article 6: Entrée en vigueur et modification dudit règlement</h6>
<p>Ce règlement entrera en vigueur le 28-10-2021 Toute modification ultérieure du règlement interne sera soumise à 
    la procédure définie dans le code du travail.</p>

</div>
</div>


    );
};

export default Reglement;