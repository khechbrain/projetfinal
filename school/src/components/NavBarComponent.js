
import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useEffect } from 'react/cjs/react.development';
import logo from '../img/logo.png'
import * as FaIcons from 'react-icons/fa';
import { auth, currentUser } from '../utils/firebase';
import SideBar from './sidebar/SideBar'
import{IoIosContact} from "react-icons/io"
import{GoSignOut} from "react-icons/go"
import{RiFileList3Line} from "react-icons/ri"
import{CgProfile} from "react-icons/cg"
import Notificator from './Notificator';

const NavBarComponent = () => {

    let navigate = useNavigate()
    const signOut=()=>{
        auth.signOut()
        // navigate('/login')
        window.location.reload()
    }
    return (
        <div className='navbar-div row mx-0 px-0' style={{zIndex:"10"}}>
        
            <div className='col-1 d-flex d-md-none me-2'>
                <div className='navbar'>
                    <SideBar></SideBar>
                </div>
            </div>
            <div className='col-10 col-md-12'>
                <nav className="navbar navbar-expand-md navbar-dark">
                    <div className="container-fluid">
                        <a className="navbar-brand" href="#">
                        <img src={logo} alt="" width="48" height="48" className="d-inline-block align-text-top"/>
                        </a>
                        <h3  style={{color:'#006bbd',fontSize:'2vw'}}>DAARA JI ACADEMY</h3>
                        <button className="navbar-toggler m-0 p-0" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
                            <img className='navbar-profile-img' src={currentUser.imageUrl}></img>
                        </button>
                        
                        <div className="collapse navbar-collapse" id="navbarScroll">
                        <ul className="navbar-nav ms-auto  my-0 my-lg-0 navbar-nav-scroll">
                            {currentUser.statut==="student"? (<Notificator></Notificator>):(currentUser.fonction==="proffessor"?(<Notificator></Notificator>):(""))}                            
                            <li className="nav-item dropdown">
                            <a className="nav-lin dropdown-toggle navbar-profile-name" style={{textDecoration:'none',border:"none"}} href="#" id="navbarScrollingDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <img className='navbar-profile-img mx-2' src={currentUser.imageUrl} style={{width:"40px",height:'40px'}}></img>
                                {currentUser.displayName}
                            </a>
                            <ul className="dropdown-menu" aria-labelledby="navbarScrollingDropdown">
                                <li>
                                    <Link to="updateProfile" className='text-decoration-none'>
                                        <a className="dropdown-item" href="#"><CgProfile/> Mon compte</a>
                                    </Link>
                                </li>
                                <li>
                                    <Link to="reglement" className='text-decoration-none'>
                                        <a className="dropdown-item" href="#"><RiFileList3Line/> Reglement intérieure</a>
                                    </Link>
                                </li>
                                <li><hr className="dropdown-divider"/></li>
                                <li style={{background:'#006bbd'}}>
                                    <button onClick={signOut} className="dropdown-item text-light"><GoSignOut/> Déconnexion</button>
                                </li>
                            </ul>
                            </li>
                        </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    );
};

export default NavBarComponent;