import format from "date-fns/format";
import getDay from "date-fns/getDay";
import parse from "date-fns/parse";
import startOfWeek from "date-fns/startOfWeek";
import {onSnapshot, collection} from "firebase/firestore"
import { Component } from 'react';
import React,{useState,useEffect} from 'react';
import "react-datepicker/dist/react-datepicker.css";
import { firestore } from "../../../utils/firebase";
import { Calendar, dateFnsLocalizer } from "react-big-calendar";
import "react-big-calendar/lib/css/react-big-calendar.css";


class Calendrier extends Component{
    state={
        allEvents:[]
    }
    componentDidMount() {
        onSnapshot(collection(firestore,"events"),
        (snapshot)=>{
            snapshot.forEach((snap)=>{
                let newEvent = snap.data()
                this.setState({allEvents:[...this.state.allEvents,newEvent]})
            })
        },
        (err)=>{
            alert(err)
        })
    }
    
    render(){
        const locales = {
            "en-US": require("date-fns/locale/en-US"),
        };
        const localizer = dateFnsLocalizer({
            format,
            parse,
            startOfWeek,
            getDay,
            locales,
        });
        
        const events = [
            {
                title: "Big Meeting",
                allDay: true,
                start: new Date(2022, 1, 12),
                end: new Date(2022, 2, 12),
            },
            {
                title: "Vacation",
                start: new Date(2022, 1, 12),
                end: new Date(2022, 2, 12),
            },
            {
                title: "Conference",
                start: new Date(2021, 6, 20),
                end: new Date(2021, 6, 23),
            },
        ];

            return (
                <div className="App">   
                    <Calendar localizer={localizer} events={this.state.allEvents} startAccessor="start" endAccessor="end" style={{ height: 500, margin: "10px" }} />
                </div>
            );
    }

}

export default Calendrier;
