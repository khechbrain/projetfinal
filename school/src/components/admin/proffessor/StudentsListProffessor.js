import React, { Component } from 'react';
import { Link, Route, Routes } from 'react-router-dom';
import { firestore,app, currentUser } from '../../../utils/firebase';
import { onSnapshot,collection} from 'firebase/firestore';
import UpdateUserProffessor from './UpdateUserProffessor';
import MessageDuffision from './MessageDuffision';


class StudentstProffessor extends Component{

    constructor(props) {
        super(props);
        this.state = {
            allUsers:[],
            userId:''
        }
        onSnapshot(collection(firestore,"users"),(snapshot)=>{
            snapshot.forEach((snap)=>{
                this.setState({
                    allUsers:this.state.allUsers.concat([snap])
                })
            })
        },(error)=>alert(error))
    }
    
    render(){

        const usersList =()=>{
            return(   
                <div className='row'>
                        <h2 className='titre-theme'>La liste de tous les étudiants</h2>
                    <div className='scroll-theme row m-auto justify-content-center' style={{overflowX:'hidden'}}> 
                        {allUsersList()}
                    </div>
                </div>
            )
        }
        const allUsersList =()=>{
            
            return this.state.allUsers.map((snap,index)=>{
                let id = snap.id
                let user = snap.data()
                
                if(user.statut ==='student'){   
                    return(
                        <div key={index} className=' row m-0 p-0 justify-content-center'>
                            <div className='col-12 col-sm-3 col-lg-2 m-0 p-2 d-flex align-items-center justify-content-center'>
                                <div style={{background:'#d7e6f5',width:'100%',maxWidth:'150px',height:"150px",overflow:'hidden', display:'flex',borderRadius:'7px'}}>
                                    <img src={user.imageUrl} className='m-auto rounded shadow' style={{width:'100%',margin:'0'}}></img>
                                </div>  
                            </div>
                            <div className='col-12 col-sm-9 col-lg-10 row'>
                                <div className='col-12 text-start my-2'>
                                    <p className='col-12'>Nom complet: <b>{user.displayName}</b></p>
                                    <p className='col-12'>Email: <b>{user.email}</b></p>
                                    <p className='col-12'>Numéro Tel: <b>{user.number}</b></p>
                                    <p className='col-12'>Etudiant(e) en: <b>{user.formation.toUpperCase()}</b></p>
                                </div>
                            </div>
                        </div>
                    )
                }
            })
        }
        return (
            <div className='row my-4 mx-3'>
                {usersList()}
            </div>
        );
    }
};

export default StudentstProffessor;
