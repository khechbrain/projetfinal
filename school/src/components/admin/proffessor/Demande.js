import  {useState} from 'react';
import {addDoc,collection,doc} from 'firebase/firestore'
import {firestore, app, currentUser, } from '../../../utils/firebase'

export default function Demande() {

  const[motif, setMotif]=useState();
  const[datedebut, setDateDebut]=useState();
  const[datefin, setDateFin]=useState();

  const envoi =(e)=>{
    e.preventDefault()
    var demande = {
      motif:motif,
      debut:datedebut,
      fin:datefin,
      authorName:currentUser.displayName,
      authorImg:currentUser.imageUrl,
      statut:currentUser.statut,
      fonction:currentUser.fonction
    }
      addDoc(collection(firestore,'requests'),demande)
      .then(()=>{
        window.history.back()
      })
  }

  return (
    <div  className='row userDiv scroll-theme shadow-lg p-2 bg-light m-4' style={{overflowX:'hidden'}}>
      <form onSubmit={e=>envoi(e)}>
        <div className="p-auto m-auto row input-group" >

          <div className='row m-auto my-2'>
            <input required type="text" className='form-control' placeholder='Motif'></input>
          </div>
          <div className='row m-auto my-2'>
            <textarea required className="form-control" placeholder='Justifications...' value={motif}  rows="3" onChange={(e) =>setMotif(e.target.value)}></textarea>
          </div>

          <div className='my-2 row'>
        
            <div className='col-12 col-md-6 d-flex my-2'>
              <label className='mx-2 col-3 text-start'><strong>DATE DEBUT</strong></label>
              <input required className='form-control' value={datedebut} type='date' placeholder='Entre Date Debut' onChange={(e) =>setDateDebut(e.target.value)} />
            </div>
            <div className='col-12 col-md-6 d-flex my-2'>
              <label className='mx-2 col-3 text-start'><strong>DATE FIN</strong></label>
              <input required className='form-control' value={datefin} type='date' placeholder='Entre Date Debut' onChange={(e) =>setDateFin(e.target.value)} onChange={(e) =>setDateFin(e.target.value)}  />
            </div>
          </div> 
        <br/>
          <button type="submit"  className="btn col-6 m-auto mt-3 btn-secondary btn-dark">ENVOYER</button>

        </div>
      </form>
    </div>

  ) ;
}
