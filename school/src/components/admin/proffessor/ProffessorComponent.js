import React from 'react';
import { Route, Routes } from 'react-router-dom';
import StudentsListProffessor from './StudentsListProffessor';
import DemandeFormProffesor from './Demande'
import Accueil from './Accueil'
import Cours from './Cours'
import Calendrier from './Calendrier';
import ModulesList from './ModulesList';


const ProffessorComponent = () => {
    return (
        <div className='row'>
            <div className='col-12'>
                <Routes>
                    <Route path="" element={<Accueil></Accueil> }></Route> 
                </Routes>
                <Routes>
                    <Route path="usersList/*" element={<StudentsListProffessor></StudentsListProffessor> }></Route> 
                </Routes>
                <Routes>
                    <Route path="calendary/*" element={<Calendrier></Calendrier> }></Route> 
                </Routes>
                <Routes>
                    <Route path="demande/*" element={<DemandeFormProffesor></DemandeFormProffesor> }></Route> 
                </Routes>
                <Routes>
                    <Route path="cours/*" element={<Cours></Cours> }></Route> 
                </Routes>
                <Routes>
                    <Route path="modules/*" element={<ModulesList></ModulesList> }></Route> 
                </Routes>
            </div>   
        </div>
    );
};

export default ProffessorComponent;