import React, { Component } from 'react';
import {app, firestore, auth, currentUser} from '../../../utils/firebase';
import { setDoc,doc, collection, onSnapshot, deleteDoc } from 'firebase/firestore';
import { deleteUser,getAuth, signInWithEmailAndPassword} from 'firebase/auth';


class UpdateUserProffessor extends Component{
    constructor(props) {
        super(props);
        
        this.state = {
            user : null,
            // fName: currentUser.fName,
            // lName: currentUser.lName,
            // number: currentUser.number,
            // email: currentUser.email,
            // password: currentUser.password,
            // statut: currentUser.statut,
            // isUpdated: false,
            // isAdmin: currentUser.statut==='admin'? (true):(false),
            // fDuration: currentUser.statut==='admin'? (""):(currentUser.fDuration),
            // fonction: currentUser.statut==='admin'? (currentUser.fonction):(''),
            // salary: currentUser.statut==='admin'?(currentUser.salary):(''),
            // formation: currentUser.statut==='admin'? (''):(currentUser.formation)
        }
    }
    componentDidMount() {
        onSnapshot(doc(firestore,"users",this.props.userId),(snap)=>{
            let currentUser = snap.data()
            this.setState({
                fName: currentUser.fName,
                lName: currentUser.lName,
                number: currentUser.number,
                email: currentUser.email,
                password: currentUser.password,
                fPassword: currentUser.password,
                statut: currentUser.statut,
                isUpdated: false,
                isAdmin: currentUser.statut==='admin'? (true):(false),
                fDuration: currentUser.statut==='admin'? (""):(currentUser.fDuration),
                fonction: currentUser.statut==='admin'? (currentUser.fonction):(''),
                salary: currentUser.statut==='admin'?(currentUser.salary):(''),
                formation: currentUser.statut==='admin'? (''):(currentUser.formation)
            })
        },(error)=>alert(error))
    }
    
    

    render(){

    
        const changeStatut=(value)=>{
            this.setState(()=>{   
                if(value==="admin"){
                    return {
                        statut:value,
                        isAdmin:true
                    }
                }else{
                    return {
                        statut:value,
                        isAdmin:false
                    }
                }
            })
        }
        const addUser =(e)=>{
            e.preventDefault(); // Stop the form from submitting
            let user = this.state.isAdmin? ({
                statut:this.state.statut,
                displayName:this.state.fName+' '+this.state.lName,
                number:this.state.number,
                email:this.state.email,
                fonction:this.state.fonction,
                salary:this.state.salary,
                modules:["photoshop"],//??????????????????
                password:this.state.password
            })
            :
            ({
                statut:this.state.statut,
                displayName:this.state.fName+' '+this.state.lName,
                number:this.state.number,
                email:this.state.email,
                formation:this.state.formation,
                fDuration:this.state.fDuration,
                paids:"0",
                modules:["photoshop"],/////?????????????
                password:this.state.password
            })
               
        }

        const removeUserCompt =()=>{
            let id = this.props.userId
            let userEmail = currentUser.email
            let userPassword = currentUser.password

            onSnapshot(doc(firestore,"users",id),(snapshot)=>{
                userEmail = snapshot.data().email
                userPassword = snapshot.data().password
            },(error)=>alert(error))

            deleteDoc(doc(firestore,"users",id)).then(()=>{
                let adminEmail = currentUser.email
                let adminPassword = currentUser.password
                signInWithEmailAndPassword(auth,userEmail,userPassword).then((result)=>{
                    let user = auth.currentUser
                    deleteUser(user).then((result)=>{
                        signInWithEmailAndPassword(auth,adminEmail,adminPassword).then((result)=>{
                            window.location.reload()
                        },(error)=>alert(error))
                    },(error)=>alert(error))
                },(error)=>alert(error))
            }).catch(error=>alert(error))
        }
        
        return (
            <div className='row my-2 justify-content-center'>
                <div className='bg-info row my-5 p-2' style={this.state.isUpdated?({display:'flex'}):({display:'none'})}>
                    <h3 className='col-11 text-light'>L'uttilisateur a bien été ajouté !</h3>
                    <button className='col-1 btn btn-danger' onClick={()=>{}}>X</button>
                </div>
                <form onSubmit={e=>addUser(e)} className='col-6 row bg-light py-3 border border-dark shadow rounded justify-content-center'>
                    <h3>Modification du profile de l'uttilisateur:</h3>
                    <input required placeholder='Prénom' className='col-6 me-3 my-2 rounded border-dark' type="text" value={this.state.fName} 
                                onChange={e=> this.setState({fName:e.target.value})}>
                    </input>
                    <input required placeholder='Nom' className='col-5 ms-3 my-2 rounded border-dark' type="text" value={this.state.lName} 
                                onChange={e=> this.setState({lName:e.target.value})}>
                    </input>
                    <input required placeholder='Numéro de téléphone' className='col-12 my-2 rounded border-dark' type="number" value={this.state.number} 
                                onChange={e=> this.setState({number:e.target.value})}>
                    </input>
                    <input required placeholder='Email' className='col-12 my-2 rounded border-dark' type="email" value={this.state.email}  
                                onChange={e=> this.setState({email:e.target.value})}>
                    </input>
                    <input required placeholder='Mot de passe' className='col-12 my-2 rounded border-dark' type="password" value={this.state.password} 
                                onChange={e=> this.setState({password:e.target.value})}>
                    </input>
        
                    <div className='row'>
                        <p className='text-start col-6 my-2'>Inscrire en tant que:</p>
                        <select className='col-6 my-2' onClick={e=>changeStatut(e.target.value)}>
                            <optgroup>
                                <option selected={this.state.isAdmin? true:false} value="admin">Admin</option>
                                <option selected={this.state.isAdmin? false:true} value="student">Etudiant</option>
                            </optgroup>
                        </select>
                    </div>
                    
                    <div className='row' style={this.state.isAdmin? ({display:'none'}):({display:'flex'})}>  
                        <p className='text-start col-6 my-2'>Type de formation:</p>
                        <select className='col-6 my-2' onClick={e=>{
                            this.setState({formation: e.target.value})
                        }}>
                            <optgroup>
                                <option selected={this.state.formation==='design'? true:false} value="design">Design</option>
                                <option selected={this.state.formation==='programation'? true:false} value="programation">Programation</option>
                                <option selected={this.state.formation==='marketing'? false:true} value="marketing">Marketing Digital</option>
                            </optgroup>
                        </select>
                        <p className='text-start col-6 my-2'>Durée de la formation:</p>
                        <select className='col-6 my-2' onClick={e=>{
                            this.setState({fDuration: e.target.value})
                        }}>
                            <optgroup>
                                <option selected={this.state.fDuration==='6'? true:false} value="6">6 mois</option>
                                <option selected={this.state.fDuration==='12'? true:false} value="12">12 mois</option>
                                <option selected={this.state.fDuration==='24'? true:false} value="24">24 mois</option>
                            </optgroup>
                        </select>
                    </div>
                    <div className='row' style={this.state.isAdmin? ({display:'flex'}):({display:'none'})}>
                        <p className='text-start col-6 my-2'>Fonction:</p>
                        <select className='col-6 my-2' onClick={e=>{
                            this.setState({fonction: e.target.value})
                        }}>
                            <optgroup>
                                <option selected={
                                        this.state.fonction==='coach'? (true):(false)
                                    }
                                    value="coach">Coaching</option>
                                <option selected={
                                        this.state.fonction==='seceretary'? (true):(false)
                                    }
                                    value="seceretary">Seceretariat</option>
                                <option selected={
                                        this.state.fonction==='security'? (true):(false)
                                    }
                                    value="security">Sécurité</option>
                            </optgroup>
                        </select>
                    </div>
                    <div className='row' style={this.state.isAdmin? ({display:'flex'}):({height:'5px',opacity:'0',zIndex:'0'})}>
                        <p className='text-start col-6 my-2'>Salaire en FCFA:</p>
                        <input placeholder='Montant' className='col-6 my-2 rounded border-dark' type="number" value={this.state.salary}
                                onChange={e=> this.setState({salary:e.target.value})}></input>
                    </div>
                    <button type='submit' className='btn col-10 btn-dark my-2' style={{zIndex:'1'}}>Mettre a jour les données</button>
                    <button onClick={removeUserCompt} className='btn col-10 btn-danger my-2' style={{zIndex:'1'}}>Supprimer de la liste des uttilisateurs</button>
                </form>
            </div>
        );
    }
};

export default UpdateUserProffessor;