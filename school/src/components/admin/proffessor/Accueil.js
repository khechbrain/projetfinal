import React, { Component } from 'react';
import 'material-icons/iconfont/material-icons.css';
import { collection, doc, onSnapshot } from 'firebase/firestore';
import { firestore } from '../../../utils/firebase';
import {GoFileSubmodule} from "react-icons/go"
import {IoIosPaper} from "react-icons/io"
import  {FaUserGraduate} from 'react-icons/fa'
import './Accueil.css'
import Calendrier from './Calendrier';
import { Link } from 'react-router-dom';


export default class  Accueil extends Component {
  state={
    moduleNumb:0,
    coursNumb:0,
    studentsNumb:0,
  }

  componentDidMount(){
    onSnapshot(collection(firestore,'modules'),
    (snapshot)=>{
      snapshot.forEach((snap)=>{
       this.setState({moduleNumb:this.state.moduleNumb+1})

      })
    })

    onSnapshot(collection(firestore,'cours'),
    (snapshot)=>{
      snapshot.forEach((snap)=>{
       this.setState({coursNumb:this.state.coursNumb+1})

      })
    })
    
    onSnapshot(collection(firestore,'users'),
    (snapshot)=>{
      snapshot.forEach((snap)=>{
        let user=snap.data()
        if (user.statut==="student"){
          this.setState({studentsNumb:this.state.studentsNumb+1})
        }

      })
    })
  }

  
  
render(){
  return(
   
      
   
          <div className="col-12 py-3 prof" style={{borderRadius:"30px"}}>
            <div className='row'>
            <div className='col'>
           
              <div className='card my-3' >
              <Link to="modules" style={{textDecoration:"none"}}>
                  <div className="text-center" style={{fontSize:"40px", marginTop:"20px", color:"white"}} ><GoFileSubmodule/></div>
                  <div className="h5"  style={{textAlign:'center', color:"white"}} >{this.state.moduleNumb}  MODULES</div>
                  </Link>
                  </div>
            

            </div>
            <div className='col'>
                <div className='card my-3'>
                <Link to="cours" style={{textDecoration:"none"}}>
                <div className="text-center" style={{fontSize:"40px",marginTop:"20px" , color:"white"}} ><IoIosPaper/> </div>
                 <div className="h5" style={{ color:"white"}} >{this.state.coursNumb} COURS</div>
                 </Link>  
 
              </div>
            </div>
            
          
             <div className='col'>
             <div className='card card my-3'>
             <Link to="usersList" style={{textDecoration:"none"}}>
             <div className="text-center" style={{fontSize:"40px",marginTop:"20px" , color:"white"}} ><FaUserGraduate/> </div>
              <div className="h5"  style={{ color:"white"}}> {this.state.studentsNumb} ETUDIANTS</div>
              </Link>     
              </div>
             </div>

            
            
             </div>
             <div className='row'> 
             <div className='col-12 border  shadow '>
          <div className='row justify-content-center bg-light'>
            <Calendrier></Calendrier>
          </div>
        </div>
             </div>
             </div>
            
             
       
    
     
    
    

  );
}

}
