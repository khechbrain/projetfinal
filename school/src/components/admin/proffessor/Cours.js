import React, { Component } from 'react';
import { Link, Route, Routes } from 'react-router-dom';
import { firestore,app, currentUser } from '../../../utils/firebase';
import { onSnapshot,collection,setDoc, doc, deleteDoc} from 'firebase/firestore';
import 'material-icons/iconfont/material-icons.css';


class Cours extends Component{

    constructor(props) {
        super(props);
        this.state = {
            coursList:[],
            coursId:''
        }
        onSnapshot(collection(firestore,"cours"),(snapshot)=>{
            snapshot.forEach((snap)=>{
                this.setState({
                    coursList:this.state.coursList.concat([snap])
                })
            })
        },(error)=>alert(error))

    }
    
    render(){

        const coursList =()=>{
            return(
                <div className='row'>
                    {coursListButtons()}
                        <h2 className='titre-theme'>La liste de tous les cours</h2>
                    <div className='row scroll-theme justify-content-center m-auto' style={{overflowX:'hidden'}}> 
                        {allCoursList()}
                    </div>
                </div>
            )
        }
        const coursListButtons =()=>{
            return(   
                <div className='row m-auto mb-3 justify-content-center'> 
                    <div className='col-12 col-md-7 col-lg-5 mx-3'>
                        <Link to="addCours"><button className='btn btn-dark col-12'>Ajouter un nouveau cours</button></Link>
                    </div>
                </div>
            )
        }

        const allCoursList =()=>{
            return this.state.coursList.map((snap,index)=>{
                let id = snap.id
                let cours = snap.data()
                if(cours.domain === currentUser.domain){
                    return(
                        <div key={index} className='row cours-item border-secondary p-0 m-0 align-items-center'>
                            <div className='col-12 text-start overflow-hidden'>
                                <h3>Titre: {cours.name}</h3>
                                <p>{cours.description}</p>
                                <a className='text-nowrap' href={cours.link} target="_blank">
                                <p style={{cursor:'pointer'}}>{cours.link}</p>
                                </a>
                                <h3 style={{fontSize:'12px',fontWeight:"bold"}}>Durée: {cours.duration}H</h3>
                            </div>
                        </div>
                       
                    )
                }
            })
        }
        return (
            <div id='courslist' className='row mx-auto my-2 justify-content-center'>
                {coursList()}
            </div>
        );
    }
};

export default Cours;