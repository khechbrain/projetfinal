import React, { Component } from 'react';
import { Link, Route, Routes } from 'react-router-dom';
import { firestore,app, currentUser } from '../../../utils/firebase';
import { onSnapshot,collection,setDoc, doc, deleteDoc} from 'firebase/firestore';




class ModulesList extends Component{

    constructor(props) {
        super(props);
        this.state = {
            allModules:[],
            moduleId:''
        }
        onSnapshot(collection(firestore,"modules"),(snapshot)=>{
            snapshot.forEach((snap)=>{
                this.setState({
                    allModules:this.state.allModules.concat([snap])
                })
            })
        },(error)=>alert(error))
    }
    
    render(){

        const modulesList =()=>{
            return(   
                <div className='row'>
                    <h2 className='titre-theme'>La liste de tous les modules</h2>
                    <div className='row scroll-theme justify-content-center m-auto' style={{overflowX:"hidden"}}> 
                       
                        {allModulesList()}
                    </div>
                </div>
            )
        }
        
        const allModulesList =()=>{  
            if(this.state.allModules.length===0){
                return <h2>Pas de module pour le moment</h2>
            }
            return this.state.allModules.map((snap,index)=>{
                let id = snap.id
                let module = snap.data()
                if(module.domain === currentUser.domain){
                    return(
                        <div key={index} className=' row m-0 p-0 justify-content-center'>
                            <div className='col-12 col-sm-3 col-lg-2 m-0 p-2 d-flex align-items-center justify-content-center'>
                                <div style={{background:'#d7e6f5',width:'100%',maxWidth:'150px',height:"150px",overflow:'hidden', display:'flex',borderRadius:'7px'}}>
                                    <img src={module.imageUrl} className='m-auto rounded shadow' style={{width:'100%',margin:'0'}}></img>
                                </div>  
                            </div>
                            <div className='col-12 col-sm-9 col-lg-10 row'>
                                <div className='col-12 text-start my-2'>
                                    <p className='col-12'>Titre: <b>{module.title}</b></p>
                                     <p className='col-12'>Description: <b>{module.description}</b></p>
                                     <a href={module.link} target="_blank">
                                         <p style={{cursor:'pointer'}}>{module.link}</p>
                                     </a>
                                     <p className='col-12'>Domaine: <b>{module.domain.toUpperCase()}</b></p>
                                </div>
                            </div>
                        </div>
                    )
                }
             
            })
        }
        return (
            <div id='moduleslist' className='row my-4 mx-3'>
                {modulesList()}
            </div>
        );
    }
};

export default ModulesList;
