import { getDatabase, onValue, ref } from 'firebase/database';
import React, { useEffect } from 'react';
import { Route, Routes, useNavigate } from 'react-router-dom';
import { app, currentUser } from '../../utils/firebase';
import ProffessorComponent from './proffessor/ProffessorComponent';
import SeceretaryComponent from './seceretary/SeceretaryComponent';

const AdminPage = () => {
    
    let navigate = useNavigate()

    // Cette partie cest pour verifier la connexion internet
    // Ne modifiez pas cette page
    useEffect(()=>{
        onValue(ref(getDatabase(app),"test"),(snapshot)=>{
            if(currentUser.fonction==='seceretary'){
                navigate('seceretary')
                console.log(currentUser);
            }else if(currentUser.fonction==='proffessor'){
                navigate('proffessor')
                console.log(currentUser);
            }else if(currentUser.fonction==='security'){
                navigate('security')
                console.log(currentUser);
            }
        },(error)=>{
            alert('Echec du chargement veuillez verifier votre connexion internet')
        })
    },[])
    // ######################################################//

    return (
        <div className='row mx-auto justify-content-center'>
                <Routes>
                    <Route path="seceretary/*" element={<SeceretaryComponent></SeceretaryComponent> }></Route> 
                    <Route path="proffessor/*" element={<ProffessorComponent></ProffessorComponent> }></Route> 
                    <Route path="security/*" element={<SeceretaryComponent></SeceretaryComponent> }></Route> 
                </Routes>
        </div>
    );
};

export default AdminPage;