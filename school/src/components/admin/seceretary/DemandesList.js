import { collection, doc, onSnapshot, setDoc } from 'firebase/firestore';
import React, { Component, useEffect, useState } from 'react';
import { firestore } from '../../../utils/firebase';

class DemandesList extends Component {

    state={
        demandesList:[]
    }

    componentDidMount() {
        onSnapshot(collection(firestore,"requests"),(snapshot)=>{
            snapshot.forEach((snap)=>{
                let demande = snap.data()
                if(demande.state === 'waiting'){
                    this.setState({demandesList:[...this.state.demandesList,snap]})
                }
            })
        })
    }
    
    render(){
       
        const updateDemangeState=(demande,id,x)=>{
            demande.state = x
            setDoc(doc(firestore,"requests",id),demande)
            .then(()=>{
                reloadComponent()
            })
        }
        const reloadComponent=()=>{
            this.setState({demandesList:[]})
            onSnapshot(collection(firestore,"requests"),(snapshot)=>{
                snapshot.forEach((snap)=>{
                    let demande = snap.data()
                    if(demande.state === 'waiting'){
                        this.setState({demandesList:[...this.state.demandesList,snap]})
                    }
                })
            })
        }
        const showDemandesList=()=>{
            return this.state.demandesList.map((snap,index)=>{
                let demande =snap.data()
                return (
                    <div key={index} className='row my-2 m-0 p-0 justify-content-center'>
                        <div className='col-12 col-sm-3 col-lg-2 m-0 p-2 d-flex align-items-center justify-content-center'>
                            <div style={{background:'#d7e6f5',width:'100%',maxWidth:'150px',height:"150px",overflow:'hidden', display:'flex',borderRadius:'7px'}}>
                                <img src={demande.authorImg} className='m-auto rounded shadow' style={{width:'100%',margin:'0'}}></img>
                            </div>  
                        </div>
                        <div className='col-12 col-sm-9 col-lg-8 row'>
                            <div className='col-12 text-start my-2 row overflow-hidden'>
                                <p className='col-12'>Nom complet: <b>{demande.authorName}</b></p>
                                <p className='col-12 text-nowrap'>Motif: <b>{demande.motif}</b></p>
                                <p className='col-12 text-nowrap'>Justification: <b>{demande.desc}</b></p>
                                <p className='col-6'>Date debut: <b>{demande.debut}</b></p>
                                <p className='col-6'>Date de fin: <b>{demande.fin}</b></p>
                            </div>
                        </div>
                        <div className='row my-2 col-12 col-sm-12 col-lg-2 justify-content-center'>
                            <div className='col-4 col-sm-3 col-md-3 col-lg-12 mx-2 row m-auto ms-lg-auto d-flex align-items-center justify-content-center'>
                                <button  className= 'm-auto ms-lg-auto btn btn-dark' onClick={e=>updateDemangeState(demande,snap.id,"yes")}>Accepter</button>
                            </div>
                            <div className='col-4 col-sm-3 col-md-3 col-lg-12 mx-2 row m-auto ms-lg-auto d-flex align-items-center justify-content-center'>
                                <button className= 'm-auto ms-lg-auto btn btn-dark' onClick={e=>updateDemangeState(demande,snap.id,"no")}>Refuser</button>
                            </div>
                        </div>
                    </div>
                )
            })
        }
        return (
            <div className='row my-4 me-md-5 m-auto mx-3'>
                <h2 className='titre-theme m-2'>Liste des demandes d'absences (en attente)</h2>
                <div className='scroll-theme m-2 ' style={{overflowX:'hidden'}}>
                    {showDemandesList()}
                </div>
            </div>
        );
    }
};

export default DemandesList;