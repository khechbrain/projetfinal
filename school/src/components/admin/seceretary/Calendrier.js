import format from "date-fns/format";
import getDay from "date-fns/getDay";
import parse from "date-fns/parse";
import startOfWeek from "date-fns/startOfWeek";
import { collection, onSnapshot } from "firebase/firestore";
import React, { Component } from "react";
import { Calendar, dateFnsLocalizer } from "react-big-calendar";
import "react-big-calendar/lib/css/react-big-calendar.css";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { firestore } from "../../../utils/firebase";
import "./Calendrier.css";

class Calendrier extends Component{
    state={
        allEvents:[]
    }
    componentDidMount() {
        onSnapshot(collection(firestore,"events"),
        (snapshot)=>{
            snapshot.forEach((snap)=>{
                let newEvent = snap.data()
                this.setState({allEvents:[...this.state.allEvents,newEvent]})
            })
        },
        (err)=>{
            alert(err)
        })
    }
    
    render(){
        const locales = {
            "en-US": require("date-fns/locale/en-US"),
        };
        const localizer = dateFnsLocalizer({
            format,
            parse,
            startOfWeek,
            getDay,
            locales,
        });

            return (
                <div className="App">   
                    <Calendar selectable messages={{
                    next: "Suivant",
                    previous: "Précédent",
                    today: "Aujourd'hui",
                    month: "Mois",
                    week: "Semaine",
                    day: "Jour"
                  }} culture='fr' localizer={localizer} events={this.state.allEvents} startAccessor="start" endAccessor="start" style={{ height: 500, margin: "10px" }} />
                </div>
            );
    }

}

export default Calendrier;