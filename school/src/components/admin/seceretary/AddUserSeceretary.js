import React, { Component, useState } from 'react';
import {app,auth,currentUser,firestore} from '../../../utils/firebase';
import {createUserWithEmailAndPassword,getAuth, signInWithEmailAndPassword} from 'firebase/auth';
import {doc,setDoc,collection, addDoc, onSnapshot} from 'firebase/firestore';
import { useNavigate } from 'react-router-dom';
import { getStorage, ref, uploadBytesResumable } from 'firebase/storage';

class AddSUserSeceretary extends Component{

    state={
            statut:"default",
            fName:"",
            lName:"",
            displayName:"",
            number:"",
            email:"",
            fonction:"default",
            domain:"default",
            password:"",
            confirmPassword:"",
            formation:"default",
            fDuration:"default",
            imageUrl: "https://firebasestorage.googleapis.com/v0/b/projet-final-bakeli.appspot.com/o/users%2Fprofiles%2Fdefault_profile_img_for_all_users.png?alt=media",
            isAdmin: false,
            allUsersEmails:[],
            hasError:false,
            errorMsg:""
    }
    componentDidMount() {
        onSnapshot(collection(firestore,"users"),(snapshot)=>{
            snapshot.forEach((snap)=>{
                this.setState({allUsersEmails:[...this.state.allUsersEmails,snap.data().email]})
            })
        })
    }
    
    render(){
        const verification=()=>{
                if(this.state.number.length !== 9 || this.state.number.substring(0,2)!=='77'&&
                this.state.number.substring(0,2)!=='78'&& this.state.number.substring(0,2)!=='76'&&
                this.state.number.substring(0,2)!=='70'){
                    this.setState({
                        hasError:true,
                        errorMsg:"Numéro de Téléphone invalid"
                    })
                    return false
                }else if(this.state.password !== this.state.confirmPassword){
                    this.setState({
                        hasError:true,
                        errorMsg:"Les deux mots de passes ne se correspondent pas"
                    })
                    return false
                }else if(this.state.statut==='default'){
                    this.setState({
                        hasError:true,
                        errorMsg:"Vous devez d'abord choisir la statut de l'utilisateur"
                    })
                    return false
                }else if(this.state.statut==='admin' && this.state.fonction==="default"){
                    this.setState({
                        hasError:true,
                        errorMsg:"Vous devez d'abord choisir la fonction de l'utilisateur"
                    })
                    return false
                }else if(this.state.statut==='student' && this.state.formation==="default"){
                    this.setState({
                        hasError:true,
                        errorMsg:"Vous devez d'abord choisir la domaine de formation de l'étudiant"
                    })
                    return false
                }else if(this.state.statut==='admin' && this.state.fonction==="proffessor" && this.state.domain ==="default"){
                    this.setState({
                        hasError:true,
                        errorMsg:"Vous devez d'abord choisir une domaine d'enseignement pour ce professeur"
                    })
                    return false
                }else{
                    let isFound = false
                    this.state.allUsersEmails.map((email,index)=>{
                        if(email===this.state.email){
                            this.setState({
                                hasError:true,
                                errorMsg:"l'email \""+this.state.email+"\" déja utilisé par un autre utilisateur"
                            })
                            isFound =true
                            return false
                        }
                    })
                    if(!isFound){
                        // Pret pour valider le formulaire
                        this.setState({hasError:false})
                        return true
                    }
                }

        }
        const changeStatut=(value)=>{
            this.setState({statut:value})
            if(value==="admin"){
                this.setState({isAdmin:true})
            }else{
                this.setState({isAdmin:false})
            }
        }
        const addUser =(e)=>{
            e.preventDefault(); // Stop the form from submitting
            let user = this.state.isAdmin? ({
                statut:this.state.statut,
                fName:this.state.fName,
                lName:this.state.lName,
                displayName:this.state.fName+' '+this.state.lName,
                number:this.state.number,
                email:this.state.email,
                fonction:this.state.fonction,
                imageUrl: this.state.imageUrl,
                domain : this.state.domain,
                password:this.state.password
            })
            :
            ({
                statut:this.state.statut,
                fName:this.state.fName,
                lName:this.state.lName,
                displayName:this.state.fName+' '+this.state.lName,
                number:this.state.number,
                email:this.state.email,
                formation:this.state.formation,
                fDuration:this.state.fDuration,
                imageUrl: this.state.imageUrl,
                password:this.state.password
            })
            if(verification()){
                // Si la vérification renvoie true
                // On peut ajouter le nouveau utilisateur
                addDoc(collection(firestore,"users"),user)
                .then((reult)=>{
                    this.props.reloadComponent()
                    window.history.back()
                }).catch((err)=>alert(err))
            }
        }
        return (
            <div>
                <h3 className='titre-theme'>Modification du profile de l'uttilisateur:</h3>
                <div className='p-2 shadow' style={{border:'4px solid #2dcdcf',borderRadius:'10px'}}>
            
                    <form onSubmit={e=>addUser(e)}>
                        <div className={this.state.hasError?'d-flex bg-warning row m-1 rounded':'d-none'}>
                            <p className='col-11'>{this.state.errorMsg}</p>
                            <button type='button' className='col-1 btn-danger rounded' onClick={()=>this.setState({hasError:false})}>X</button>
                        </div>
                        <div className='row justify-content-center my-1'>
                            <div className='col-12 col-sm-6 justify-content-start'>
                                <div className='input-group col-6'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Prénom:</label>
                                    <input type="text" required className='col-9 my-2 rounded form-control' placeholder='Prénom' value={this.state.fName} onChange={e=>this.setState({fName:e.target.value})}></input>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 '>
                                <div className='input-group col-6'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Nom:</label>
                                    <input type="text" required className='col-9 my-2 rounded form-control' placeholder='Nom' value={this.state.lName} onChange={e=>this.setState({lName:e.target.value})}></input>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 '>
                                <div className='input-group col-6'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Numéro:</label>
                                    <input type="number" required className='col-9 my-2 rounded form-control' placeholder='Numéro de téléphone' value={this.state.number} onChange={e=>this.setState({number:e.target.value})}></input>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 '>
                                <div className='input-group col-6'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Email:</label>
                                    <input type="email" required className='col-9 my-2 rounded form-control' placeholder='Email' value={this.state.email} onChange={e=>this.setState({email:e.target.value})}></input>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 col-md-9 col-lg-6 '>
                                <div className='input-group col-6'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Mot de passe:</label>
                                    <input type="password" required className='col-9 my-2 rounded form-control' placeholder='Mot de passe' value={this.state.password} onChange={e=>this.setState({password:e.target.value})}></input>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 col-md-9 col-lg-6 '>
                                <div className='input-group col-6'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Confirmer le Mot de passe:</label>
                                    <input type="password" required className='col-9 my-2 rounded form-control' placeholder='Mot de passe' value={this.state.confirmPassword} onChange={e=>this.setState({confirmPassword:e.target.value})}></input>
                                </div>
                            </div>
                        </div>
                        <div className='row justify-content-center my-1 align-items-center'>
                            <div className='col-12 col-sm-6 my-2'>
                                <div className='input-group col-5'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Statut:</label>
                                    <select className='col-8' onChange={e=>changeStatut(e.target.value)}>
                                        <optgroup>
                                            <option className='text-center' selected value="default" disabled>--Veillez choisir--</option>
                                            <option  value="admin">Admin</option>
                                            <option  value="student">Etudiant</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 my-2' style={this.state.isAdmin? ({display:'none'}):({display:'flex'})}>
                                <div className='input-group col-5'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3 col-sm-5 col-md-4 col-lg-3'>Domaine de formation:</label>
                                    <select className=' col-8 col-sm-6 col-md-7 col-lg-8' onChange={e=>{
                                        this.setState({formation: e.target.value})
                                    }}>
                                        <optgroup>
                                            <option className='text-center' selected value="default" disabled>--Veillez choisir--</option>
                                            <option value="design">Design</option>
                                            <option value="programation">Programation</option>
                                            <option value="marketing">Marketing Digital</option>
                                            <option value="gestion">Gestion de projet</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 my-2' style={this.state.isAdmin? ({display:'none'}):({display:'flex'})}>
                                <div className='input-group col-5'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3 col-sm-5 col-md-4 col-lg-3'>Durée de la formation:</label>
                                    <select className=' col-8 col-sm-6 col-md-7 col-lg-8' onChange={e=>{
                                        this.setState({fDuration: e.target.value})
                                    }}>
                                        <optgroup>
                                            <option className='text-center' selected value="default" disabled>--Veillez choisir--</option>
                                            <option value="3">3 mois</option>
                                            <option value="6">6 mois</option>
                                            <option value="12">12 mois</option>
                                            <option value="24">24 mois</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 my-2' style={this.state.isAdmin? ({display:'flex'}):({display:'none'})}>
                                <div className='input-group col-5'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Fonction:</label>
                                    <select className='col-8' onChange={e=>{
                                        this.setState({fonction: e.target.value})
                                    }}>
                                        <optgroup>
                                            <option className='text-center' selected value="default" disabled>--Veillez choisir--</option>
                                            <option value="proffessor">Professeur</option>
                                            <option value="seceretary">Seceretariat</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 my-2' style={this.state.isAdmin && this.state.fonction ==="proffessor"? ({display:'flex'}):({display:'none'})}>
                                <div className='input-group col-5'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3 col-sm-5 col-md-4 col-lg-3'>Domaine d'enseignement:</label>
                                    <select className=' col-8 col-sm-6 col-md-7 col-lg-8' onChange={e=>{
                                        this.setState({domain: e.target.value})
                                    }}>
                                        <optgroup>
                                            <option className='text-center' selected value="default" disabled>--Veillez choisir--</option>
                                            <option value="design">Design</option>
                                            <option value="programation">Programation</option>
                                            <option value="marketing">Marketing Digital</option>
                                            <option value="gestion">Gestion de projet</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <button type='submit' className='btn col-10 col-sm-6 col-md-8 col-lg-6  btn-dark my-2' style={{zIndex:'1'}}>Ajouter un utilisateur</button>
                    </form>
                </div>
            </div>
        );
    }
};

export default AddSUserSeceretary;