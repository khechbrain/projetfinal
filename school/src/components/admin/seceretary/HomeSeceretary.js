import React,{useState,useEffect} from 'react';
import  {FaUserGraduate} from 'react-icons/fa'
import {GiTeacher} from 'react-icons/gi'
import {onSnapshot, collection} from "firebase/firestore"
import {firestore} from "../../../utils/firebase"
import { Component } from 'react';
import {IoIosPaper} from "react-icons/io"
import {GoFileSubmodule} from "react-icons/go"
import Calendrier from "./Calendrier"
import {Link} from "react-router-dom"
import './Accueil.css'
export default class HomeSeceretary extends Component{
  state={
    studentsNumb:0,
    profNumb:0,
    moduleNumb:0,
    coursNumb:0
  }
  constructor(props) {
    super(props);
    
    onSnapshot(collection(firestore,"users"),(snapshot)=>{
      snapshot.forEach((snap)=>{
        let user=snap.data()
        if (user.statut==="student"){
          this.setState({studentsNumb:this.state.studentsNumb+1})
        }
        if(user.statut==="admin" && user.fonction==="proffessor"){
          this.setState({ profNumb:this.state.profNumb+1})
        }
       
      })

    })
    onSnapshot(collection(firestore,'cours'),
    (snapshot)=>{
      snapshot.forEach((snap)=>{
       this.setState({coursNumb:this.state.coursNumb+1})

      })
    })
    onSnapshot(collection(firestore,'modules'),
    (snapshot)=>{
      snapshot.forEach((snap)=>{
       this.setState({moduleNumb:this.state.moduleNumb+1})  

      })
    })
  }
  
  render(){
    return (
      <div className="row mx-auto mt-3 bg-light secretaire justify-content-center" >
      
      <div className="row justify-content-center">
          <div className="col-12 col-sm-6 col-lg-3" >
              <div className='card my-3' >
                <Link to="studentsList" style={{textDecoration:"none"}}>
              <div className="text-center" style={{fontSize:"40px",marginTop:"20px", color:"white"}} ><FaUserGraduate/> </div>
               <div className="h6" style={{color:"white"}}> {this.state.studentsNumb}</div>
                     
               </Link>  
              </div>
           
        </div> 
        <div className="col-12 col-sm-6 col-lg-3" >
              <div className='card my-3' >
              <Link to="proflist"style={{textDecoration:"none"}}>
              <div className="text-center" style={{fontSize:"40px",marginTop:"20px", color:"white"}} ><GiTeacher/> </div>
                      <div className="h6" style={{color:"white"}}> {this.state.profNumb}</div>
                </Link>   
              </div>
           
        </div> 
        <div className="col-12 col-sm-6 col-lg-3" >
              <div className='card my-3' >
                <Link to="modules"style={{textDecoration:"none"}}>
              <div className="text-center" style={{fontSize:"40px", marginTop:"20px", color:"white"}} ><GoFileSubmodule/></div>
                      <div className="h6" style={{color:"white"}}> {this.state.moduleNumb}</div>
                      </Link>    
              </div>
           
        </div> 
        <div className="col-12 col-sm-6 col-lg-3" >
              <div className='card my-3' >
                <Link to="cours"style={{textDecoration:"none"}}>
              <div className="text-center" style={{fontSize:"30px",marginTop:"20px", color:"white"}} ><IoIosPaper/> </div>
                      <div className="h6" style={{color:"white"}}> {this.state.coursNumb}</div>
                      </Link>   
              </div>
           
           
        </div>   <Calendrier></Calendrier> 
      </div>
      </div>
  
  
    );
  }

 
}
