import React, { Component } from 'react';
import {Link, Route, Routes} from 'react-router-dom'
import { collection, deleteDoc, doc, onSnapshot } from 'firebase/firestore';
import { firestore } from '../../../utils/firebase';
import AddMessageDuffision from './AddMessageDuffision';

class MessageDifList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            messagesList:[],
            messageId:''
        }
        onSnapshot(collection(firestore,"messages"),(snapshot)=>{
            snapshot.forEach((snap)=>{
                this.setState({
                    messagesList:this.state.messagesList.concat([snap])
                })
            })
        },(error)=>alert(error))
    }
    
    render() {
        const reloadComponent=()=>{
            
            this.setState({messagesList:[]})
            onSnapshot(collection(firestore,"messages"),(snapshot)=>{
                snapshot.forEach((snap)=>{
                    this.setState({
                        messagesList:this.state.messagesList.concat([snap])
                    })
                })
            },(error)=>alert(error))
        }
        const updateMessage=(id)=>{
            this.setState({messageId:id})
        }
        const deteleMessage=(id)=>{
            if(window.confirm("Voulez-vous reelement supprimer ce message")){
                deleteDoc(doc(firestore,"messages",id))
                .then((result) =>{
                   //    var sup = window.document.getElementById("courslist");
                   //    sup.remove();
                   reloadComponent()
                })
                .catch((err) =>{
                   alert(err)
                })
               }
        }
        const allMessages=()=>{
            return this.state.messagesList.map((snap,index)=>{
                let id = snap.id
                let message = snap.data()
                return(
                  
                    <div key={index} className='row cours-item border-secondary my-2 p-0 m-0 align-items-center'>
                        <div className='col-12 col-sm-10 col-md-10 col-lg-10 text-start'>
                            <h3>{message.title}</h3>
                            <p>{message.description}</p>
                            <p>Envoyé par: {message.authorName}</p>
                        </div>

                        <div className='col-12 col-sm-2 col-md-2 col-lg-2 ms-auto'>
                            <div className='row col-6 col-sm-12 col-md-10 col-lg-6 m-auto ms-lg-auto justify-content-center'>
                                <button onClick={()=>updateMessage(id)} className= 'fixed-btn col-4 col-sm-12 col-md-12 col-lg-9 m-2 ms-auto btn btn-dark'>
                                    <Link to="updateCours" className='col-12 m-auto text-light'>
                                        <span className="material-icons-outlined fs-3 m-auto">
                                        edit
                                        </span>
                                    </Link>
                                </button>
                                <button onClick={()=>deteleMessage(id)} className= 'fixed-btn col-4 col-sm-12 col-md-12 col-lg-9 m-2 ms-auto btn btn-dark'>
                                    <span className="material-icons-outlined fs-3 m-auto">
                                        delete_forever
                                    </span>
                                </button>

                            </div>
                        </div>
                        
                    </div>
                   
                )
            })
        }
        return (
            <Routes>
                <Route path="" element={
                    <div className='justify-content-center row px-4 py-2'>
                        <div className='col-5 mx-3 my-1 col-12 col-md-6 col-lg-5'>
                            <Link to="addMessage"><button className='btn btn-dark col-12'>Envoyer un message de diffusion</button></Link>
                        </div>
                        <h2 className='titre-theme'>La liste des messages recents</h2>
                        <div className='scroll-theme' style={{overflowX:'hidden'}}>
                            {allMessages()}
                        </div>
                    </div>
                }></Route>
                <Route path='addMessage' element={<AddMessageDuffision reloadComponent={reloadComponent}></AddMessageDuffision>}></Route>
            </Routes>
        );
    }
}

export default MessageDifList;