import { getDate } from 'date-fns';
import { addDoc, collection, doc, onSnapshot, setDoc, } from 'firebase/firestore';
import React, { useState } from 'react';
import { currentUser, firestore } from '../../../utils/firebase';

class AddMessageDuffision extends React.Component{
    state = {
        title:"",
        desc:'',
        destination:'',
        isStudentBoxChecked:false,
        isProfBoxChecked:false,
        hasError:false,
        errorMsg:""
    }
    
    render(){
        const verification=()=>{
            if(this.state.isStudentBoxChecked && this.state.isProfBoxChecked){
                return true
            }else if(this.state.isStudentBoxChecked){
                return true
            }else if(this.state.isProfBoxChecked){
                return true
            }else{
                this.setState({
                    hasError:true,
                    errorMsg:'Veuillez spécifier a qui le message est destiné'
                })
                return false
            }
        }

        const sendMessagge = (e)=>{
            e.preventDefault()

            let myMessage = {
                title:this.state.title,
                description:this.state.desc,
                authorName:currentUser.displayName,
                date: new Date(),
                destination: this.state.isStudentBoxChecked && this.state.isProfBoxChecked?('all'):(this.state.isStudentBoxChecked?'student':'prof') 
            }
            if(verification()){
                addDoc(collection(firestore,"messages"),myMessage)
                .then(()=>{
                    this.props.reloadComponent();
                    window.history.back()
                })
            }
        }
        return (
            <div className='col-12 my-2 mx-auto row'>
                <h3 className='titre-theme'>Message de diffusion destiné a tous les étudiants</h3>
                <div className='scroll-theme' style={{overflowX:'hidden'}}>
                    <form onSubmit={e=>sendMessagge(e)}>

                        <div className={this.state.hasError?'d-flex bg-warning row m-1 rounded':'d-none'}>
                            <p className='col-11'>{this.state.errorMsg}</p>
                            <button type='button' className='col-1 btn-danger rounded' onClick={()=>this.setState({hasError:false})}>X</button>
                        </div>

                        <div className='my-3'>
                            <input required type='text' className='form-control' placeholder='Titre' value={this.state.title} onChange={e=> this.setState({title:e.target.value})}></input>
                        </div>
                        <div className='my-3'>
                            <textarea required rows="6" className='form-control' placeholder='Description: Exp: Bonjours chers étudiants...'
                                value={this.state.desc} onChange={e=>this.setState({desc:e.target.value})}></textarea>
                        </div>
                        <div className='row'>
                            <p className='col-12 col-sm-6 col-md-3 text-start'>Message déstiné a tous les:</p>
                            <div className='d-inline fw-bold col-6 col-sm-3 col-md-4 ms-auto'>
                                <label>
                                    <input type="checkbox" onChange={e=>this.setState({isStudentBoxChecked:!this.state.isStudentBoxChecked})}></input>
                                    Etudiants
                                </label>
                            </div>
                            <div className='d-inline fw-bold col-6 col-sm-3 col-md-4'>
                                <label>
                                    <input type="checkbox" onChange={e=>this.setState({isProfBoxChecked:!this.state.isProfBoxChecked})}></input>
                                    Professeurs
                                </label>
                            </div>
                        </div>
                        <button type='submit' className='btn btn-dark my-3 col-12 col-sm-6'>Evoyer</button>
                    </form>
                </div>
            </div>
        );
    }
};

export default AddMessageDuffision;