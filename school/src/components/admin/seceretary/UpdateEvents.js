import { doc, onSnapshot, setDoc } from 'firebase/firestore';
import React, { Component } from 'react';
import { firestore } from '../../../utils/firebase';

class UpdateEvents extends Component {
  state={
    title:'',
    start:'',
    end:''
  }
  componentDidMount() {
    onSnapshot(doc(firestore,'events',this.props.eventsId),
    (snap)=>{
      let event = snap.data()
      this.setState({
        title:event.title,
        start:event.start,
        end:event.end
      })
    })
  }
  render() {
    const submit=(e)=>{
      e.preventDefault()
      let event = {
        title:this.state.title,
        start:this.state.start,
        end:this.state.end
      }
      setDoc(doc(firestore,"events",this.props.eventsId),event)
      .then((result)=>{
        window.history.back()
        this.props.reloadComponent()
      })
      .catch((err)=>{
        alert(err)
      })
    }
    return (
      <div className='row bg-light border my-5 p-3 mx-auto rounded shadow justify-content-center'>
          <form onSubmit={e=>submit(e)} className='row mx-auto'>
            <textarea className='col-12 border border-dark rounded' rows='5' type="text" placeholder='Titre' value={this.state.title} onChange={e=>this.setState({title:e.target.value})}></textarea>
            <div className='row my-3 justify-content-center'>
              <input className='col-10 col-lg-5 me-auto my-2 border border-dark rounded' type="date" placeholder='Date de debut' value={this.state.start} onChange={e=>this.setState({start:e.target.value})}></input>
              <input className='col-10 col-lg-5 ms-auto my-2 border border-dark rounded' type="date" placeholder='Date de fin' value={this.state.end} onChange={e=>this.setState({end:e.target.value})}></input>
            </div>
            <div className='row mx-auto justify-content-center'>
              {/* <ParticleEffectButton color='#121019' hidden={hidden} color='#006bbd' duration={3000}> */}
                <button type='submit' className='btn btn-dark col-12' style={{width:'50vw'}}>Ajouter</button>
              {/* </ParticleEffectButton> */}
            </div>
          </form>
      </div>
    );
  }
}

export default UpdateEvents;