import React, { Component } from 'react';
import { Link, Route, Routes } from 'react-router-dom';
import AddEventsForm from './AddEventsForm';
import { firestore } from '../../../utils/firebase';
import { onSnapshot,collection,setDoc, doc, deleteDoc} from 'firebase/firestore';
import UpdateEvents from './UpdateEvents';
import 'material-icons/iconfont/material-icons.css';

// import MessageDuffision from './MessageDuffision';


class EventsList extends Component{

    constructor(props) {
        super(props);
        this.state = {
            eventsList:[],
            eventsId:''
        }
        onSnapshot(collection(firestore,"events"),(snapshot)=>{
            snapshot.forEach((snap)=>{
                this.setState({
                    eventsList:this.state.eventsList.concat([snap])
                })
            })
        },(error)=>alert(error))

    }
    
    render(){

        const eventsList =()=>{
            return(
                <div className='row'>
                    {eventsListButtons()}
                        <h2 className='titre-theme'>La liste de tous les événements</h2>
                    <div className='row scroll-theme justify-content-center m-auto' style={{overflowX:'hidden'}}> 
                        {allEventsList()}
                    </div>
                </div>
            )
        }
        const eventsListButtons =()=>{
            return(   
                <div className='row m-auto mb-3 justify-content-center'> 
                    <div className='col-12 col-md-7 col-lg-5 mx-3'>
                        <Link to="addEvents"><button className='btn btn-dark col-12'>Ajouter un nouveau événement</button></Link>
                    </div>
                </div>
            )
        }

        const allEventsList =()=>{
            if(this.state.eventsList.length===0){
                return <h2>Pas d'évenement pour le moment</h2>
            }
            return this.state.eventsList.map((snap,index)=>{
                let id = snap.id
                let events = snap.data()
                
                return(
                  
                    <div key={index} className='row cours-item border-secondary p-auto py-2 mb-4 align-items-center'>
                        <div className='col-12 row col-sm-12 col-lg-10 text-start'>
                            <h2>Titre: {events.title}</h2>
                            <p className='col-12' style={{height:'25px',overflow:'hidden'}}>{events.description}</p>
                            <p className='col-6 fw-bold'>Date-debut: {events.start}</p>
                            <p className='col-6 fw-bold'>Date-fin: {events.end}</p>
                            
                        </div>

                        <div className='row my-2 col-12 col-sm-12 col-lg-2 align-items-center justify-content-center'>
                            <div className='col-3 col-sm-2 col-md-3 col-lg-10 mx-2 row my-lg-2 m-auto ms-lg-auto d-flex align-items-center justify-content-center'>
                                <button onClick={()=>updateEvents(id)} className= 'fixed-btn m-auto ms-lg-auto btn btn-dark'>
                                    <Link to="updateUser" className='m-auto text-light'>
                                        <span className="material-icons-outlined fs-3 m-auto">
                                        edit
                                        </span>
                                    </Link>
                                </button>
                            </div>
                            <div className='col-3 col-sm-2 col-md-3 col-lg-10 mx-2 my-lg-2 row m-auto ms-lg-auto d-flex align-items-center justify-content-center'>
                                <button onClick={()=>deteleEvents(id)} className= 'fixed-btn m-auto ms-lg-auto btn btn-dark'>
                                    <span className="material-icons-outlined fs-3 m-auto p-1">
                                    delete_forever
                                    </span>
                                </button>
                            </div>
                        </div> 
                    </div>
                   
                )
            })
        }
        const deteleEvents = (id) =>{
            if(window.confirm("Voulez-vous reelement supprimer cet événement")){
            this.setState({eventsList:[]})
             deleteDoc(doc(firestore,"events",id))
             .then((result) =>{
                //    var sup = window.document.getElementById("courslist");
                //    sup.remove();
                reloadComponent()
             })
             .catch((err) =>{
                alert(err)
             })
            }
         }
        const updateEvents = (id) => {
            this.setState({eventsId:id})
        };
        const reloadComponent=()=>{
            this.setState({eventsList:[]})
            onSnapshot(collection(firestore,"events"),(snapshot)=>{
                snapshot.forEach((snap)=>{
                    this.setState({
                        eventsList:this.state.eventsList.concat([snap])
                    })
                })
            },(error)=>alert(error))
        }
        return (
            <div id='eventslist' className='row mx-auto my-2 justify-content-center'>
                <Routes>
                    <Route path="" element={eventsList()}></Route>
                    <Route path="updateEvents" element={<UpdateEvents reloadComponent={reloadComponent} eventsId={this.state.eventsId}></UpdateEvents>}></Route>
                    <Route path="addEvents" element={<AddEventsForm reloadComponent={reloadComponent}></AddEventsForm> }></Route>
                </Routes>
            </div>
        );
    }
};

export default EventsList;