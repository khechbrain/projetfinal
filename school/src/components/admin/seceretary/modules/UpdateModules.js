import React, { Component } from 'react';
import {app, firestore, auth, currentUser} from '../../../../utils/firebase';
import { setDoc,doc, collection, onSnapshot, deleteDoc, getFirestore } from 'firebase/firestore';
import { getStorage, ref, uploadBytesResumable } from 'firebase/storage';

class UpdateModules extends Component{
    state = {
        title:"",
        description:"",
        domain: "",
        imageName: "",
        imageUrl: "",
        userImg: null
    }
    componentDidMount() {
        onSnapshot(doc(firestore,"modules",this.props.moduleId),(snap)=>{
            let currentModules = snap.data()
            this.setState({
                title: currentModules.title,
                description: currentModules.description,
                domain: currentModules.domain,
                imageName: currentModules.imageName,
                imageUrl: currentModules.imageUrl,
            })
        },(error)=>alert(error))
    }
    
    

    render(){
        const updateModules =(e)=>{
            e.preventDefault(); // Stop the form from submitting
            let module = {
               title : this.state.title,
               description : this.state.description,
               domain:this.state.domain,
               imageName: this.state.imageName,
               imageUrl: this.state.imageUrl,
            }
            if(this.state.userImg!==null){
                let storageRef = ref(getStorage(app),"modules/"+this.state.imageName)
                uploadBytesResumable(storageRef,this.state.userImg)
                .on("state_changed",()=>{
                })
            }
            setDoc(doc(firestore,"modules",this.props.moduleId),module).then((ressult)=>{
                this.props.reloadComponent()
                window.history.back()
            }).catch((error)=>{
                alert("error")

            })
        }
        return (
            <div>
                <h3 className='titre-theme'>Mise a jour du module:</h3>
                <div className='scroll-theme' style={{overflowX:'hidden'}}>
                    <form onSubmit={e=>updateModules(e)}>
                        <div className='row justify-content-center'>
                            <div className='row input-group my-2'>
                                <input required className='form-control' type='text' placeholder='Titre' value={this.state.title} onChange={e=>this.setState({title:e.target.value})}></input>
                            </div>
                            <div className='row input-group'>
                                <textarea required className='form-control' type='text' rows="6" placeholder='Titre' value={this.state.description} onChange={e=>this.setState({description:e.target.value})}></textarea>
                            </div>
                            <div className='col-12 col-md-6 d-flex align-items-center'>
                                <div className='col-12 input-group d-flex align-items-center'>
                                    <label className='m-2'>Domaine: </label>
                                    <select required className='m-2 form-control' onChange={e=>this.setState({domain:e.target.value})}>
                                        <optgroup>
                                            <option selected={this.state.domain==="design"? true:false} value="design">Design</option>
                                            <option selected={this.state.domain==="programation"? true:false} value="programation">Programation</option>
                                            <option selected={this.state.domain==="marketing"? true:false} value="marketing">Marketing Digital</option>
                                            <option selected={this.state.domain==="gestion"? true:false} value="gestion">Gestion de projet</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div className='col-12 col-md-6 d-flex align-items-center'>
                                <div className='col-12 input-group d-flex align-items-center'>
                                    <label className='m-2'>Image: </label>
                                    <input className='m-2 form-control' type='file' onChange={e=>this.setState({userImg:e.target.files[0]})}></input>
                                </div>
                            </div>
                            <button className='btn btn-dark col-10 col-md-5 my-2'>Modifier</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
};

export default UpdateModules;