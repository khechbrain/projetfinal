import React, { useState } from 'react';
import {uploadBytesResumable,ref, getStorage} from 'firebase/storage'
import { app, firestore } from '../../../../utils/firebase';
import { doc, setDoc } from 'firebase/firestore';

const AjoutModules = (props) => {

    const [title,setTitle] = useState()
    const [description,setDescription] = useState()
    const [domain,setDomain] = useState("default")
    const [userImg,setUserImg] = useState()
    const [hasError,setHasError] = useState(false)
    const [errorMsg,setErrorMsg] = useState("")
    let isLoaded = false
    let randomNumber = Math.round(Math.random()*100000)

    const verification=()=>{
        if(userImg.type!=='image/jpeg' && userImg.type!=='image/png'){
            setHasError(true)
            setErrorMsg("Format d'image non reconnue seuls les images de types JPEG JPG et PNG sont autorisées")
            return false
        }else if(domain==="default"){
            setHasError(true)
            setErrorMsg("Veuillez choisir un domaine pour le module de formation")
            return false
        }else{
            return true
        }
    }
    const addModule=(e)=>{
        e.preventDefault()
        let newModule = {
            title:title,
            description:description,
            domain : domain,
            imageName : randomNumber+userImg.name,
            imageUrl: "https://firebasestorage.googleapis.com/v0/b/projet-final-bakeli.appspot.com/o/modules%2F"+randomNumber+userImg.name+"?alt=media"
        }
        if(verification()){
            if(!isLoaded){
                let storageRef = ref(getStorage(app),"modules/"+randomNumber+userImg.name)
                uploadBytesResumable(storageRef,userImg)
                .on("state_changed",()=>{
                    
                })
                isLoaded = true
                setDoc(doc(firestore,"modules","module"+ randomNumber),newModule)
                .then(()=>{
                    props.reloadComponent()
                    window.history.back()
                })
            }
        }
    }
    return (
        <div>
            <h3 className='titre-theme'>Ajouter un nouveau module:</h3>
            <div className='scroll-theme' style={{overflowX:'hidden'}}>
                <form onSubmit={e=>addModule(e)}>
                    <div className={hasError?'d-flex bg-warning row m-1 rounded':'d-none'}>
                        <p className='col-11'>{errorMsg}</p>
                        <button type='button' className='col-1 btn-danger rounded' onClick={()=>setHasError(false)}>X</button>
                    </div>
                    <div className='row justify-content-center'>
                        <div className='row input-group my-2'>
                            <input required className='form-control' type='text' placeholder='Titre' value={title} onChange={e=>setTitle(e.target.value)}></input>
                        </div>
                        <div className='row input-group'>
                            <textarea required className='form-control' type='text' rows="6" placeholder='Description' value={description} onChange={e=>setDescription(e.target.value)}></textarea>
                        </div>
                        <div className='col-12 col-md-6 d-flex align-items-center'>
                            <div className='col-12 input-group d-flex align-items-center'>
                                <label className='m-2'>Domaine: </label>
                                <select required className='m-2 form-control' onChange={e=>setDomain(e.target.value)}>
                                    <optgroup>
                                        <option className='text-center' selected value="default" disabled>--Appuiez ici pour choisir--</option>
                                        <option value="design">Design</option>
                                        <option value="programation">Programation</option>
                                        <option value="marketing">Marketing Digital</option>
                                        <option value="gestion">Gestion de projet</option>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        <div className='col-12 col-md-6 d-flex align-items-center'>
                            <div className='col-12 input-group d-flex align-items-center'>
                                <label className='m-2'>Image: </label>
                                <input required className='m-2 form-control' type='file' onChange={e=>setUserImg(e.target.files[0])}></input>
                            </div>
                        </div>
                        <button className='btn btn-dark col-10 col-md-5 my-2'>Ajouter</button>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default AjoutModules;