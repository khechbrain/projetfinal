import React, { Component } from 'react';
import { Link, Route, Routes } from 'react-router-dom';
import AjoutModules from './AjoutModules';
import { firestore,app, currentUser } from '../../../../utils/firebase';
import { onSnapshot,collection,setDoc, doc, deleteDoc} from 'firebase/firestore';
import UpdateModules from './UpdateModules';
// import UpdateUserSeceretary from './UpdateUserSeceretary';
// import MessageDuffision from './MessageDuffision';


class ModulesList extends Component{

    constructor(props) {
        super(props);
        this.state = {
            allModules:[],
            moduleId:''
        }
        onSnapshot(collection(firestore,"modules"),(snapshot)=>{
            snapshot.forEach((snap)=>{
                this.setState({
                    allModules:this.state.allModules.concat([snap])
                })
            })
        },(error)=>alert(error))
    }
    
    render(){

        const modulesList =()=>{
            return(   
                <div className='row'>
                    {modulesListButtons()}
                    <h2 className='titre-theme'>La liste de tous les modules</h2>
                    <div className='row scroll-theme justify-content-center m-auto' style={{overflowX:"hidden"}}> 
                       
                        {allModulesList()}
                    </div>
                </div>
            )
        }
        const modulesListButtons =()=>{
            return(   
                <div className='row mb-3 justify-content-center'> 
                    <div className='col-5 mx-3'>
                        <Link to="addModules"><button className='btn btn-dark col-12'>Ajouter un nouveau module</button></Link>
                    </div>
                   
                </div>
            )
        }
        
        const deteleModules = (id) =>{
            if(window.confirm("Voulez-vous réelement supprimer ce module")){
             deleteDoc(doc(firestore,"modules",id))
             .then((result) =>{
                 reloadComponent()
             })
             .catch((err) =>{
                alert(err)
             })
            }
         }
        const allModulesList =()=>{  
            if(this.state.allModules.length===0){
                return <h2>Pas de module pour le moment</h2>
            }
            return this.state.allModules.map((snap,index)=>{
                let id = snap.id
                let module = snap.data()
                
                return(
                    <div key={index} className=' row m-0 p-0 justify-content-center'>
                        <div className='col-12 col-sm-3 col-lg-2 m-0 p-2 d-flex align-items-center justify-content-center'>
                            <div style={{background:'#d7e6f5',width:'100%',maxWidth:'150px',height:"150px",overflow:'hidden', display:'flex',borderRadius:'7px'}}>
                                <img src={module.imageUrl} className='m-auto rounded shadow' style={{width:'100%',margin:'0'}}></img>
                            </div>  
                        </div>
                        <div className='col-12 col-sm-9 col-lg-8 row'>
                            <div className='col-12 text-start my-2'>
                                <p className='col-12'>Titre: <b>{module.title}</b></p>
                                 <p className='col-12'>Description: <b>{module.description}</b></p>
                                 <a href={module.link} target="_blank">
                                     <p style={{cursor:'pointer'}}>{module.link}</p>
                                 </a>
                                 <p className='col-12'>Domaine: <b>{module.domain.toUpperCase()}</b></p>
                            </div>
                        </div>
                        <div className='row my-2 col-12 col-sm-12 col-lg-2 justify-content-center'>
                            <div className='col-3 col-sm-2 col-md-3 col-lg-10 mx-2 row m-auto ms-lg-auto d-flex align-items-center justify-content-center'>
                                <button onClick={()=>useUpdate(id)} className= 'fixed-btn m-auto ms-lg-auto btn btn-dark'>
                                    <Link to="updateModules" className='m-auto text-light'>
                                        <span className="material-icons-outlined fs-3 m-auto">
                                        edit
                                        </span>
                                    </Link>
                                </button>
                            </div>
                            <div className='col-3 col-sm-2 col-md-3 col-lg-10 mx-2 row m-auto ms-lg-auto d-flex align-items-center justify-content-center'>
                                <button onClick={()=>deteleModules(id)} className= 'fixed-btn m-auto ms-lg-auto btn btn-dark'>
                                    <span className="material-icons-outlined fs-3 m-auto p-1">
                                    delete_forever
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                )
             
            })
        }
        const useUpdate = (id) => {
            this.setState({moduleId:id})
        };
        const reloadComponent =()=>{
            this.setState({allModules:[]})
            onSnapshot(collection(firestore,"modules"),(snapshot)=>{
                snapshot.forEach((snap)=>{
                    this.setState({
                        allModules:this.state.allModules.concat([snap])
                    })
                })
            },(error)=>alert(error))
        }
        return (
            <div id='moduleslist' className='row my-4 mx-3'>
                <Routes>
                    <Route path="" element={modulesList()}></Route>
                    {/* <Route path="updateCours" element={<UpdateUserSeceretary userId={this.state.userId}></UpdateUserSeceretary> }></Route> */}
                    <Route path="addModules" element={<AjoutModules reloadComponent={reloadComponent}></AjoutModules> }></Route>
                    <Route path="updateModules" element={<UpdateModules reloadComponent={reloadComponent} moduleId={this.state.moduleId}></UpdateModules> }></Route>
                </Routes>
            </div>
        );
    }
};

export default ModulesList;