import React from 'react';
import { Route, Routes } from 'react-router-dom';
import StudentsListSeceretary from './StudentsListSeceretary';
import ListeCours from './cours/ListeCours';
import ModulesList from './modules/ModulesList';
import ProfListSeceretary from './ProfListSeceretary'
import EventsList from './EventsList'

import HomeSeceretary from './HomeSeceretary'
import MessageDifList from './MessageDifList';
import DemandesList from './DemandesList';


const SeceretaryComponent = () => {
    return (
        <div className='row'>
            <div className='col-12'>
                <Routes>
                    <Route path="" element={<HomeSeceretary></HomeSeceretary> }></Route> 
                </Routes>
                <Routes>
                    <Route path="studentsList/*" element={<StudentsListSeceretary></StudentsListSeceretary> }></Route> 
                </Routes>
                <Routes>
                    <Route path="cours/*" element={<ListeCours></ListeCours> }></Route> 
                </Routes>
                <Routes>
                    <Route path="modules/*" element={<ModulesList></ModulesList> }></Route> 
                </Routes>
                <Routes>
                    <Route path="proflist/*" element={<ProfListSeceretary></ProfListSeceretary>}></Route> 
                </Routes>
                 <Routes>
                    <Route path="events/*" element={<EventsList></EventsList>}></Route> 
                </Routes>
                 <Routes>
                    <Route path="messages/*" element={<MessageDifList></MessageDifList>}></Route> 
                </Routes>
                 <Routes>
                    <Route path="demandesList/*" element={<DemandesList></DemandesList>}></Route> 
                </Routes>
            </div>   
        </div>
    );
};

export default SeceretaryComponent;