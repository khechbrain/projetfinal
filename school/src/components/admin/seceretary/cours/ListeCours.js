import React, { Component } from 'react';
import { Link, Route, Routes } from 'react-router-dom';
import AjoutCours from './AjoutCours';
import { firestore,app, currentUser } from '../../../../utils/firebase';
import { onSnapshot,collection,setDoc, doc, deleteDoc} from 'firebase/firestore';
import UpdateCours from './UpdateCours';
import 'material-icons/iconfont/material-icons.css';
import './ListeCours.css';
// import MessageDuffision from './MessageDuffision';


class ListeCours extends Component{

    constructor(props) {
        super(props);
        this.state = {
            coursList:[],
            coursId:''
        }
        onSnapshot(collection(firestore,"cours"),(snapshot)=>{
            snapshot.forEach((snap)=>{
                this.setState({
                    coursList:this.state.coursList.concat([snap])
                })
            })
        },(error)=>alert(error))

    }
    
    render(){

        const coursList =()=>{
            return(
                <div className='row'>
                    {coursListButtons()}
                        <h2 className='titre-theme'>La liste de tous les cours</h2>
                    <div className='row scroll-theme justify-content-center m-auto' style={{overflowX:'hidden'}}> 
                        {allCoursList()}
                    </div>
                </div>
            )
        }
        const coursListButtons =()=>{
            return(   
                <div className='row m-auto mb-3 justify-content-center'> 
                    <div className='col-12 col-md-7 col-lg-5 mx-3'>
                        <Link to="addCours"><button className='btn btn-dark col-12'>Ajouter un nouveau cours</button></Link>
                    </div>
                </div>
            )
        }

        const allCoursList =()=>{
            return this.state.coursList.map((snap,index)=>{
                let id = snap.id
                let cours = snap.data()
                return(
                  
                    <div key={index} className='row cours-item border-secondary p-0 m-0 align-items-center'>
                        <div className='col-12 col-sm-10 col-md-10 col-lg-10 text-start overflow-hidden'>
                            <h3>Titre: {cours.name}</h3>
                            <p>{cours.description}</p>
                            <a className='text-nowrap' href={cours.link} target="_blank">
                            <p style={{cursor:'pointer'}}>{cours.link}</p>
                            </a>
                            <h3 style={{fontSize:'12px',fontWeight:"bold"}}>Durée: {cours.duration}H</h3>
                        </div>

                        <div className='col-12 col-sm-2 col-md-2 col-lg-2 ms-auto'>
                            <div className='row col-6 col-sm-12 col-md-10 col-lg-6 m-auto ms-lg-auto justify-content-center'>
                                <button onClick={()=>updateCours(id)} className= 'fixed-btn col-4 col-sm-12 col-md-12 col-lg-9 m-2 ms-auto btn btn-dark'>
                                    <Link to="updateCours" className='col-12 m-auto text-light'>
                                        <span className="material-icons-outlined fs-3 m-auto">
                                        edit
                                        </span>
                                    </Link>
                                </button>
                                <button onClick={()=>deteleCours(id)} className= 'fixed-btn col-4 col-sm-12 col-md-12 col-lg-9 m-2 ms-auto btn btn-dark'>
                                    <span className="material-icons-outlined fs-3 m-auto">
                                        delete_forever
                                    </span>
                                </button>

                            </div>
                        </div>
                        
                    </div>
                   
                )
            })
        }
        const deteleCours = (id) =>{
            if(window.confirm("Voulez-vous reelement supprimer ce cours")){
             deleteDoc(doc(firestore,"cours",id))
             .then((result) =>{
                //    var sup = window.document.getElementById("courslist");
                //    sup.remove();
                updateCoursList()
             })
             .catch((err) =>{
                alert(err)
             })
            }
         }
        const updateCours = (id) => {
            this.setState({coursId:id})
        };
        const updateCoursList =()=>{
            this.setState({coursList:[]})
            onSnapshot(collection(firestore,"cours"),(snapshot)=>{
                snapshot.forEach((snap)=>{
                    this.setState({
                        coursList:this.state.coursList.concat([snap])
                    })
                })
            },(error)=>alert(error))
        }
        return (
            <div id='courslist' className='row mx-auto my-2 justify-content-center'>
                <Routes>
                    <Route path="" element={coursList()}></Route>
                    <Route path="updateCours" element={<UpdateCours  updateCoursList={updateCoursList} coursId={this.state.coursId}></UpdateCours>}></Route>
                    <Route path="addCours" element={<AjoutCours updateCoursList={updateCoursList}></AjoutCours> }></Route>
                </Routes>
            </div>
        );
    }
};

export default ListeCours;