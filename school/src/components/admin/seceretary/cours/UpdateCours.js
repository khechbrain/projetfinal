import React, { Component } from 'react';
import {app, firestore, auth, currentUser} from '../../../../utils/firebase';
import { setDoc,doc, collection, onSnapshot, deleteDoc } from 'firebase/firestore';
import { deleteUser,getAuth, signInWithEmailAndPassword} from 'firebase/auth';
import { Link, useNavigate } from 'react-router-dom';

class UpdateCours extends Component{
    
    constructor(props) {
        super(props);
        
        this.state = {
            cours : null,
            coursTitle:"",
            desc:"",
            duration:"",
            domain:"default",
            module:"default",
            link:"",
            optionsList:[],
            hasError:false,
            errorMsg:""
        }
    }
    componentDidMount() {
        onSnapshot(doc(firestore,"cours",this.props.coursId),(snap)=>{
            let currentCours = snap.data()
            this.setState({
                coursTitle:currentCours.name,
                desc:currentCours.description,
                duration:currentCours.duration,
                domain:currentCours.domain,
                module:currentCours.module,
                link:currentCours.link,
            })
        },(error)=>alert(error))
    }
    
    

    render(){
        const updateCours =(e)=>{
            e.preventDefault(); // Stop the form from submitting
            let cours = {
               name : this.state.name,
               description : this.state.description,
               duration : this.state.duration,
               link : this.state.link,
               module : this.state.module,
            }
            
            setDoc(doc(firestore,"cours",this.props.coursId
                ),cours).then((result) =>{
                 this.props.updateCoursList()
                 window.history.back()
                }).catch((err) =>{
                    alert(err);
                })
               
        }
        const showOptions=()=>{
            return this.state.optionsList.map((option)=>{
                return <option value={option.id}>{option.title.toUpperCase()}</option>
            })
        }
        const updateDomain=(value)=>{
            this.setState({
                domain:value,
                optionsList:[]
            })
            
            onSnapshot(collection(firestore,"modules"),(snapshot)=>{
                snapshot.forEach((snap)=>{
                    let option = snap.data()
                    if(this.state.domain===option.domain){
                        this.setState({optionsList:this.state.optionsList.concat([option])})
                    }
                })
            })
        }
        return (
            <div>
            <h3 className='titre-theme'>Modification du cours:</h3>
            <div className='scroll-theme' style={{overflowX:'hidden'}}>
                <form onSubmit={e=>updateCours(e)}>

                    <div className={this.state.hasError?'d-flex bg-warning row m-1 rounded':'d-none'}>
                        <p className='col-11'>{this.state.errorMsg}</p>
                        <button type='button' className='col-1 btn-danger rounded' onClick={()=>this.setState({hasError:false})}>X</button>
                    </div>
                   
                    <div className='row justify-content-center'>
                        <div className='row input-group my-2'>
                        <input required placeholder='Titre du cours' className='form-control' type="text" value={this.state.coursTitle} onChange={e=>this.setState({coursTitle:e.target.value})}></input>
                        </div>
                        <div className='row input-group'>
                        <textarea required maxLength='100' placeholder='Description' className='form-control' type='text' rows="6"value={this.state.desc} onChange={e=>this.setState({desc:e.target.value})}></textarea>
                        <input required placeholder='Duree' className='col-12 my-2 rounded border-dark ' type="number"value={this.state.duration} onChange={e=>this.setState({duration:e.target.value})}></input>
                        <input required placeholder='Lien du cours' className='col-6 my-2 rounded border-dark form-control' type="url" value={this.state.link} onChange={e=>this.setState({link:e.target.value})}></input>
                        
                        </div>
                        <div className='col-12 col-md-6 d-flex align-items-center'>
                            <div className='col-12 input-group d-flex align-items-center'>
                                <label className='m-2 '>Domaine: </label>
                                <select className='col-6 my-2 form-control' onClick={e=>updateDomain(e.target.value)}>
                                    <optgroup>
                                        <option selected={this.state.domain==='design'? true:false} value="design">Design</option>
                                        <option selected={this.state.domain==='programation'? true:false} value="programation">Programation</option>
                                        <option selected={this.state.domain==='marketing'? true:false} value="marketing">Marketing Digital</option>
                                        <option selected={this.state.domain==='gestion'? true:false} value="gestion">Gestion de projet</option>
                                    </optgroup>
                                </select>
                               
                            </div>
                        </div>
                        <div className='col-12 col-md-6 d-flex align-items-center'>
                            <div className='col-12 input-group d-flex align-items-center'>
                            <label className='m-2 '>Module: </label>
                            <select className='col-6 my-2 form-control' defaultValue="default" onClick={e=>this.setState({module:e.target.value})}>
                                <optgroup>
                                    <option className='text-center' selected value="default" disabled>{this.state.module.toUpperCase()} </option>
                                    {showOptions()}
                                </optgroup>
                             </select>
                     
                            </div>
                        </div>
                        <button className='btn btn-dark col-10 col-md-5 my-2'>Ajouter</button>
                    </div>
                </form>
            </div>
        </div>
        );
    }
};

export default UpdateCours;