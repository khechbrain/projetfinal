import React from 'react';
// import { useNavigate } from 'react-router-dom';
import {firestore} from '../../../../utils/firebase';
// import {createUserWithEmailAndPassword,getAuth, signInWithEmailAndPassword} from 'firebase/auth';
import {collection, addDoc, onSnapshot} from 'firebase/firestore';
import "./AddCours.css";
class AddUserSeceretary extends React.Component {

    state = {
        coursTitle:"",
        desc:"",
        duration:"",
        domain:"default",
        module:"default",
        link:"",
        optionsList:[],
        hasError:false,
        errorMsg:""
    }
    constructor(props) {
        super(props);
        
        onSnapshot(collection(firestore,"modules"),(snapshot)=>{
            snapshot.forEach((snap)=>{
                let option = snap.data()
                if(this.state.domain===option.domain){
                    this.setState({optionsList:this.state.optionsList.concat([option])})
                }
            })
        })
    }
    
    render(){
        const verification = ()=>{
            if(this.state.domain==='default'){
                this.setState({
                    hasError:true,
                    errorMsg:'Veuillez d\'abord choisir une domaine pour ce cours'
                })
                return false
            }else if(this.state.module==="default"){
                this.setState({
                    hasError:true,
                    errorMsg:'Veuillez d\'abord choisir un module pour ce cours'
                })
                return false
            }else{
                this.setState({
                    hasError:false,
                    errorMsg:""
                })
                return true
            }
        }
        const addCours =(e)=>{
            e.preventDefault(); // Stop the form from submitting
            let user = {
                name : this.state.coursTitle,
                description : this.state.desc,
                duration : this.state.duration,
                domain : this.state.domain,
                module : this.state.module,
                link : this.state.link
            }
            if(verification()){
                addDoc(collection(firestore,"cours"),user)
                .then(result=>{
                    ////
                    window.history.back()
                    this.props.updateCoursList()
                }).catch(error=>{ alert(error)})
            }
        }
        
        const showOptions=()=>{
            return this.state.optionsList.map((option)=>{
                return <option value={option.id}>{option.title}</option>
            })
        }
        const updateDomain=(value)=>{
            this.setState({
                domain:value,
                optionsList:[]
            })
            
            onSnapshot(collection(firestore,"modules"),(snapshot)=>{
                snapshot.forEach((snap)=>{
                    let option = snap.data()
                    if(this.state.domain===option.domain){
                        this.setState({optionsList:this.state.optionsList.concat([option])})
                    }
                })
            })
        }
        return (
            <div>
            <h3 className='titre-theme'>Ajouter un nouveau cours:</h3>
            <div className='scroll-theme' style={{overflowX:'hidden'}}>
                <form onSubmit={e=>addCours(e)}>

                    <div className={this.state.hasError?'d-flex bg-warning row m-1 rounded':'d-none'}>
                        <p className='col-11'>{this.state.errorMsg}</p>
                        <button type='button' className='col-1 btn-danger rounded' onClick={()=>this.setState({hasError:false})}>X</button>
                    </div>
                   
                    <div className='row justify-content-center'>
                        <div className='row input-group my-2'>
                        <input required placeholder='Titre du cours' className='form-control' type="text" value={this.state.coursTitle} onChange={e=>this.setState({coursTitle:e.target.value})}></input>
                        </div>
                        <div className='row input-group'>
                        <textarea required placeholder='Description' className='form-control' type='text' rows="6"value={this.state.desc} onChange={e=>this.setState({desc:e.target.value})}></textarea>
                        <input required placeholder='Duree (H)' className='col-12 my-2 rounded border-dark ' type="number"value={this.state.duration} onChange={e=>this.setState({duration:e.target.value})}></input>
                        <input required placeholder='Lien du cours' className='col-6 my-2 rounded border-dark form-control' type="url" value={this.state.link} onChange={e=>this.setState({link:e.target.value})}></input>
                        
                        </div>
                        <div className='col-12 col-md-6 d-flex align-items-center'>
                            <div className='col-12 input-group d-flex align-items-center'>
                                <label className='m-2 '>Domaine: </label>
                                <select className='col-6 my-2 form-control' onClick={e=>updateDomain(e.target.value)}>
                                    <optgroup>
                                        <option className='text-center' selected value="default" disabled>--Appuiez ici pour choisir--</option>
                                        <option value="design">Design</option>
                                        <option value="programation">Programation</option>
                                        <option value="marketing">Marketing Digital</option>
                                        <option value="gestion">Gestion de projet</option>
                                    </optgroup>
                                </select>
                               
                            </div>
                        </div>
                        <div className='col-12 col-md-6 d-flex align-items-center'>
                            <div className='col-12 input-group d-flex align-items-center'>
                            <label className='m-2 '>Module: </label>
                            <select className='col-6 my-2 form-control' defaultValue="default" onClick={e=>this.setState({module:e.target.value})}>
                                <optgroup>
                                    <option className='text-center' selected value="default" disabled>--Appuiez ici pour choisir--</option>
                                    {showOptions()}
                                </optgroup>
                             </select>
                     
                            </div>
                        </div>
                        <button className='btn btn-dark col-10 col-md-5 my-2'>Ajouter</button>
                    </div>
                </form>
            </div>
        </div>
            
        );
    }
};

export default AddUserSeceretary;