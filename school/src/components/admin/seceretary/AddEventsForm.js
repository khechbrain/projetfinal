import { addDoc, collection } from 'firebase/firestore'
import React, { useState } from 'react'
import { firestore } from '../../../utils/firebase'
import ParticleEffectButton from 'react-particle-effect-button'

export default function AddEventsForm(props) {
  const [hidden,setHidden] = useState(false)

  const [title,setTitle] = useState("")
  const [desc,setDesc] = useState("")
  const [dateStart,setDateStart] = useState(null)
  const [dateEnd,setDateEnd] = useState(null)

  const annimateButton =()=>{
    setHidden(!hidden)
  }
  const submit=(e)=>{
    e.preventDefault()
    annimateButton()
    let event = {
      title:title,
      description:desc,
      start:dateStart,
      end:dateEnd
    }
    addDoc(collection(firestore,"events"),event)
    .then((result)=>{
      props.reloadComponent()
      window.history.back()
    })
    .catch((err)=>{
      alert(err)
    })
  }
  return (
      <div className='row bg-light border my-5 p-3 mx-auto rounded shadow justify-content-center'>
          <form onSubmit={e=>submit(e)} className='row mx-auto'>
            <input required className='my-3' type="text" placeholder='Titre' value={title} onChange={e=>setTitle(e.target.value)}></input>
            <textarea required className='col-12 border border-dark rounded' rows='5' type="text" placeholder='Description' value={desc} onChange={e=>setDesc(e.target.value)}></textarea>
            <div className='row my-3 justify-content-center'>
              <input required className='col-10 col-lg-5 me-auto my-2 border border-dark rounded' type="date" placeholder='Date de debut' value={dateStart} onChange={e=>setDateStart(e.target.value)}></input>
              <input required className='col-10 col-lg-5 ms-auto my-2 border border-dark rounded' type="date" placeholder='Date de fin' value={dateEnd} onChange={e=>setDateEnd(e.target.value)}></input>
            </div>
            <div className='row mx-auto justify-content-center'>
              {/* <ParticleEffectButton color='#121019' hidden={hidden} color='#006bbd' duration={3000}> */}
                <button type='submit' className='btn btn-dark col-12' style={{width:'50vw'}}>Ajouter</button>
              {/* </ParticleEffectButton> */}
            </div>
          </form>
      </div>
  )
}
