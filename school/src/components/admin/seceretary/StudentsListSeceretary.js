import React, { Component } from 'react';
import { Link, Route, Routes } from 'react-router-dom';
import AddSUserSeceretary from './AddUserSeceretary';
import { firestore} from '../../../utils/firebase';
import { onSnapshot,collection, deleteDoc, doc} from 'firebase/firestore';
import UpdateUserSeceretary from './UpdateUserSeceretary';
import 'material-icons/iconfont/material-icons.css';


class StudentsListSeceretary extends Component{

    constructor(props) {
        super(props);
        this.state = {
            allUsers:[],
            userId:''
        }
        onSnapshot(collection(firestore,"users"),(snapshot)=>{
            snapshot.forEach((snap)=>{
                this.setState({
                    allUsers:this.state.allUsers.concat([snap])
                })
            })
        },(error)=>alert(error))
    }
    
    render(){

        const usersList =()=>{
            return(   
                <div className='row'>
                    {usersListButtons()}
                        <h2 className='titre-theme'>La liste de tous les étudiants</h2>
                    <div className='scroll-theme row m-auto justify-content-center' style={{overflowX:'hidden'}}> 
                        {allUsersList()}
                    </div>
                </div>
            )
        }
        const usersListButtons =()=>{
            return(   
                <div className='row mb-3 justify-content-center'> 
                    <div className='mx-3 my-1 col-12 col-md-6 col-lg-5'>
                        <Link to="addUser"><button className='btn btn-dark col-12'>Ajouter un nouveau étudiant</button></Link>
                    </div>
                </div>
            )
        }
        const deleteUser=(userId)=>{
            if(window.confirm("Voulez vous vraiment supprimer cet étudinat?")){
                deleteDoc(doc(firestore,"users",userId)).then(()=>{
                    reloadComponent()
                })
            }
        }
        const allUsersList =()=>{
            
            return this.state.allUsers.map((snap,index)=>{
                let id = snap.id
                let user = snap.data()
                
                if(user.statut ==='student'){   
                    return(
                        <div key={index} className=' row m-0 p-0 justify-content-center'>
                            <div className='col-12 col-sm-3 col-lg-2 m-0 p-2 d-flex align-items-center justify-content-center'>
                                <div style={{background:'#d7e6f5',width:'100%',maxWidth:'150px',height:"150px",overflow:'hidden', display:'flex',borderRadius:'7px'}}>
                                    <img src={user.imageUrl} className='m-auto rounded shadow' style={{width:'100%',margin:'0'}}></img>
                                </div>  
                            </div>
                            <div className='col-12 col-sm-9 col-lg-8 row'>
                                <div className='col-12 text-start my-2'>
                                    <p className='col-12'>Nom complet: <b>{user.displayName}</b></p>
                                    <p className='col-12'>Email: <b>{user.email}</b></p>
                                    <p className='col-12'>Numéro Tel: <b>{user.number}</b></p>
                                    <p className='col-12'>Etudiant(e) en: <b>{user.formation.toUpperCase()}</b></p>
                                </div>
                            </div>
                            <div className='row my-2 col-12 col-sm-12 col-lg-2 justify-content-center'>
                                <div className='col-3 col-sm-2 col-md-3 col-lg-10 mx-2 row m-auto ms-lg-auto d-flex align-items-center justify-content-center'>
                                    <button onClick={()=>useUpdate(id)} className= 'fixed-btn m-auto ms-lg-auto btn btn-dark'>
                                        <Link to="updateUser" className='m-auto text-light'>
                                            <span className="material-icons-outlined fs-3 m-auto">
                                            edit
                                            </span>
                                        </Link>
                                    </button>
                                </div>
                                <div className='col-3 col-sm-2 col-md-3 col-lg-10 mx-2 row m-auto ms-lg-auto d-flex align-items-center justify-content-center'>
                                    <button onClick={()=>deleteUser(id)} className= 'fixed-btn m-auto ms-lg-auto btn btn-dark'>
                                        <span className="material-icons-outlined fs-3 m-auto p-1">
                                        delete_forever
                                        </span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    )
                }
            })
        }
        const useUpdate = (id) => {
            this.setState({userId:id})
        };
       
        const reloadComponent =()=>{
            this.setState({allUsers:[]})
            onSnapshot(collection(firestore,"users"),(snapshot)=>{
                snapshot.forEach((snap)=>{
                    this.setState({
                        allUsers:this.state.allUsers.concat([snap])
                    })
                })
            },(error)=>alert(error))
        }
        
        return (
            <div className='row my-4 mx-3'>
                <Routes>
                    <Route path="" element={usersList()}></Route>
                    <Route path="updateUser" element={<UpdateUserSeceretary reloadComponent={reloadComponent} userId={this.state.userId}></UpdateUserSeceretary> }></Route>
                    <Route path="addUser" element={<AddSUserSeceretary reloadComponent={reloadComponent}></AddSUserSeceretary> }></Route>
                </Routes>
            </div>
        );
    }
};

export default StudentsListSeceretary;