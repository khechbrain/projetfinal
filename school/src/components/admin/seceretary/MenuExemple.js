import React from 'react';
import { Link } from 'react-router-dom';

const MenuExemple = () => {
    return (
        <div className='menu'>
            <ul>
                <Link to="seceretary" className='text-decoration-none'><p className='col-12 bg-dark my-2  text-light'>Accueil</p></Link>
                <Link to="usersList" className='text-decoration-none'><p className='col-12 bg-dark my-2  text-light'>Etudiants</p></Link>
                <Link to="cours" className='text-decoration-none'><p className='col-12 bg-dark my-2  text-light'>Cours</p></Link>
                <Link to="form" className='text-decoration-none'><p className='col-12 bg-dark my-2  text-light'>Comptablité</p></Link>
                <Link to="add" className='text-decoration-none'><p className='col-12 bg-dark my-2  text-light'>Statistiques</p></Link>
            </ul>
        </div>
    );
};

export default MenuExemple;