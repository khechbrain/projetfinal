import React, { Component } from 'react';
import {app, firestore} from '../../../utils/firebase';
import { setDoc,doc, onSnapshot } from 'firebase/firestore';


class UpdateUserSeceretary extends Component{
    constructor(props) {
        super(props);
        
        this.state = {
            user : null
        }
    }
    componentDidMount() {
        onSnapshot(doc(firestore,"users",this.props.userId),(snap)=>{
            let currentUser = snap.data()
            this.setState({
                fName: currentUser.fName,
                lName: currentUser.lName,
                number: currentUser.number,
                email: currentUser.email,
                imageUrl: currentUser.imageUrl,
                password: currentUser.password,
                fPassword: currentUser.password,
                statut: currentUser.statut,
                isUpdated: false,
                domain: currentUser.statut==='admin'? (currentUser.domain):("design"),
                isAdmin: currentUser.statut==='admin'? (true):(false),
                fDuration: currentUser.statut==='admin'? (""):(currentUser.fDuration),
                fonction: currentUser.statut==='admin'? (currentUser.fonction):('proffessor'),
                salary: currentUser.statut==='admin'?(currentUser.salary):(''),
                formation: currentUser.statut==='admin'? (''):(currentUser.formation),
                userImg:null
            })
        },(error)=>alert(error))
    }
    
    

    render(){

    
        const changeStatut=(value)=>{
            this.setState(()=>{
                if(value==="admin"){
                    return {
                        statut:value,
                        isAdmin:true
                    }
                }else{
                    return {
                        statut:value,
                        isAdmin:false
                    }
                }
            })
        }
        const updateUser =(e)=>{
            e.preventDefault(); // Stop the form from submitting
            let user = this.state.isAdmin? ({
                statut:this.state.statut,
                fName:this.state.fName,
                lName:this.state.lName,
                displayName:this.state.fName+' '+this.state.lName,
                number:this.state.number,
                email:this.state.email,
                fonction:this.state.fonction,
                domain : this.state.domain,
                imageUrl: this.state.imageUrl,
                salary:this.state.salary,
                password:this.state.password
            })
            :
            ({
                statut:this.state.statut,
                fName:this.state.fName,
                lName:this.state.lName,
                displayName:this.state.fName+' '+this.state.lName,
                number:this.state.number,
                email:this.state.email,
                formation:this.state.formation,
                domain : this.state.domain,
                fDuration:this.state.fDuration,
                imageUrl: this.state.imageUrl,
                password:this.state.password
            })

            let id = this.props.userId
            
            setDoc(doc(firestore,'users',id),user)
            .then((result)=>{
                this.props.reloadComponent()
                window.history.back()
            },(error)=>alert(error))
        }
         
        return (
            <div>
                <h3 className='titre-theme'>Modification du profile de l'uttilisateur:</h3>
                <div className='p-2 shadow' style={{border:'4px solid #2dcdcf',borderRadius:'10px'}}>

                    <form onSubmit={e=>updateUser(e)}>
                        <div className='row justify-content-center my-1'>
                            <div className='col-12 col-sm-6 justify-content-start'>
                                <div className='input-group col-6'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Prénom:</label>
                                    <input type="text" required className='col-9 my-2 rounded form-control' placeholder='Prénom' value={this.state.fName} onChange={e=>this.setState({fName:e.target.value})}></input>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 '>
                                <div className='input-group col-6'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Nom:</label>
                                    <input type="text" required className='col-9 my-2 rounded form-control' placeholder='Nom' value={this.state.lName} onChange={e=>this.setState({lName:e.target.value})}></input>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 '>
                                <div className='input-group col-6'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Numéro:</label>
                                    <input type="number" required className='col-9 my-2 rounded form-control' placeholder='Numéro de téléphone' value={this.state.number} onChange={e=>this.setState({number:e.target.value})}></input>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 '>
                                <div className='input-group col-6'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Email:</label>
                                    <input type="email" required className='col-9 my-2 rounded form-control' placeholder='Email' value={this.state.email} onChange={e=>this.setState({email:e.target.value})}></input>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 col-md-9 col-lg-6 '>
                                <div className='input-group col-6'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Mot de passe:</label>
                                    <input type="password" required className='col-9 my-2 rounded form-control' placeholder='Mot de passe' value={this.state.password} onChange={e=>this.setState({password:e.target.value})}></input>
                                </div>
                            </div>
                        </div>
                        <div className='row justify-content-center my-1 align-items-center'>
                            <div className='col-12 col-sm-6 my-2'>
                                <div className='input-group col-5'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Statut:</label>
                                    <select className='col-8' onChange={e=>changeStatut(e.target.value)}>
                                        <optgroup>
                                            <option selected={this.state.isAdmin? true:false} value="admin">Admin</option>
                                            <option selected={this.state.isAdmin? false:true} value="student">Etudiant</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 my-2' style={this.state.isAdmin? ({display:'none'}):({display:'flex'})}>
                                <div className='input-group col-5'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3 col-sm-5 col-md-4 col-lg-3'>Domaine de formation:</label>
                                    <select className=' col-8 col-sm-6 col-md-7 col-lg-8' onClick={e=>{
                                        this.setState({formation: e.target.value})
                                    }}>
                                        <optgroup>
                                            <option selected={this.state.formation==='design'? true:false} value="design">Design</option>
                                            <option selected={this.state.formation==='programation'? true:false} value="programation">Programation</option>
                                            <option selected={this.state.formation==='marketing'? true:false} value="marketing">Marketing Digital</option>
                                            <option selected={this.state.formation==='gestion'? true:false} value="gestion">Gestion de projet</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 my-2' style={this.state.isAdmin? ({display:'flex'}):({display:'none'})}>
                                <div className='input-group col-5'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3'>Fonction:</label>
                                    <select className='col-8' onClick={e=>{
                                        this.setState({fonction: e.target.value})
                                    }}>
                                        <optgroup>
                                            <option selected={
                                                    this.state.fonction==='proffessor'? (true):(false)
                                                }
                                                value="proffessor">Professeur</option>
                                            <option selected={
                                                    this.state.fonction==='seceretary'? (true):(false)
                                                }
                                                value="seceretary">Seceretariat</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div className='col-12 col-sm-6 my-2' style={this.state.isAdmin && this.state.fonction ==="proffessor"? ({display:'flex'}):({display:'none'})}>
                                <div className='input-group col-5'style={{alignItems:'center'}}>
                                    <label className='mx-1 col-3 col-sm-5 col-md-4 col-lg-3'>Domaine d'enseignement:</label>
                                    <select required className=' col-8 col-sm-6 col-md-7 col-lg-8' onChange={e=>{
                                        this.setState({domain: e.target.value})
                                    }}>
                                        <optgroup>
                                            <option value="design">Design</option>
                                            <option value="programation">Programation</option>
                                            <option value="marketing">Marketing Digital</option>
                                            <option value="gestion">Gestion de projet</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <button type='submit' className='btn col-10 col-sm-6 col-md-8 col-lg-6  btn-dark my-2' style={{zIndex:'1'}}>Mettre a jour les données</button>
                    </form>
                </div>
            </div>
        );
    }
};

export default UpdateUserSeceretary;